function validarCuadrilla(){
    var are_id_a = document.getElementById('are_id_a').value;
    var cdl_fecha_a = document.getElementById('cdl_fecha_a').value;

    if(are_id_a==="" || cdl_fecha_a===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false; 
    }
    if(are_id_a ==="0" || are_id_a ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un area de trabajo por favor'           
          })
          return false; 
    }else if(cdl_fecha_a === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar una fecha'           
          })
          return false; 
    }else{
        Swal.fire({
            icon: 'success',
            title: 'Modificacion exitosa...',
            text: 'La modificacion se ha realizado con exito'           
          })
    }
}