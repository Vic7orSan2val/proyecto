function addCuadrilla(){   

    
    var are_id_a = document.getElementById('are_id_a').value;
    var cdl_fecha_a = document.getElementById('cdl_fecha_a').value;

    if(are_id_a == 0 && cdl_fecha_a===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false; 
    }
    if(are_id_a ==0 || are_id_a ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un area de trabajo por favor'           
          })
          return false; 
    }else if(cdl_fecha_a === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar una fecha'           
          })
          return false; 
    }




    else{
        // alert('Todo bien');
    cadena = "are_id_a=" + are_id_a + "&cdl_fecha_a=" + cdl_fecha_a ;

    $.ajax({
        type: "POST",
        url: "crud/agregar-cuadrilla.php",
        data:  cadena,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes-c/tablac.php');
                //alert("Agregado con exito");
                Swal.fire({
                    icon: 'success', 
                    title: 'Registro exitoso',
                    text: "La marca se ha registrado con éxito",

                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.href = "../cuadrilla/listado-cuadrilla.php";
                    }
                })
            }
            else{
               // alert("ERROR, NO AGREGADO");
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error',
                    text: 'Debe completar todos los campos'
                    
                })
            }
        }
    });
}
}


// function datosform(datos){
       
//     // alert(datos);
//     d = datos.split('||');
//     $('#cdl_id').val(d[0]);
//     $('#are_id_e').val(d[1]);
//     $('#cdl_fecha_e').val(d[2]);

// }

// function modificararea(){ 
//     cdl_id = $('#cdl_id').val(),
//     are_id_e=  $('#are_id_e').val(),
//     cdl_fecha_e = $('#cdl_fecha_e').val()


//     cadena =    "cdl_id="+cdl_id+
//                 "&are_id_e=" + are_id_e +                   
//                 "&cdl_fecha_e="+cdl_fecha_e;
                
//                 $.ajax({
//                     type: "POST",
//                     url: "crud/editar-cuadrilla.php",
//                     data:  cadena,
//                     success: function(r){
//                         if(r==1){
//                             $('#tabla').load('componentes-c/tablac.php');
//                             //alert("Agregado con exito");
//                             Swal.fire({
//                                 icon: 'success',
//                                 title: 'Registro exitoso',
//                                 text: "La marca se ha registrado con éxito",
            
//                                 showCancelButton: false,
//                                 confirmButtonColor: '#3085d6',
//                                 cancelButtonColor: '#d33',
//                                 confirmButtonText: 'Continuar'
//                             }).then((result) => {
//                                 if (result.value) {
//                                     location.href = "../cuadrila/listado-cuadrilla.php";
//                                 }
//                             })
//                         }
//                         else{
//                            // alert("ERROR, NO AGREGADO");
//                             Swal.fire({
//                                 icon: 'error',
//                                 title: 'Ha ocurrido un error',
//                                 text: 'Debe completar todos los campos'
//                             })
//                         }
//                     }
//                 });
                
//             }

 
 function confirmarEliminar(cdl_id){
    
                // alertify.confirm('Elminar Tipo','¿Seguro que desea eliminar?', 
                // function()
                // {   eliminarTipo(cdl_id)},
                //     // alertify.success('Si') } ,
                // function(){
                //     alertify.message('Eliminación cancelada')});
                // // alert('hola')
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: true
                    })
                
                    swalWithBootstrapButtons.fire({
                    title: '¿Está seguro que desea eliminar la cuadrilla de trabajo?',
                    text: "No podrá recuperar el registro guardado!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: '¡Sí, eliminar!',
                    cancelButtonText: '¡No, cancelar!',
                    reverseButtons: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    }).then((result) => {
                    if (result.value) {
                        eliminarTipo(cdl_id)
                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                        'Cancelado',
                        'La cuadrilla no se eliminará',
                        'error'
                        )
                    }
                    })

                
}
        
        function eliminarTipo(cdl_id){
            cadena="cdl_id=" + cdl_id;
        
            $.ajax({
                type: "POST",
                url: 'crud/eliminar-cuadrilla.php' ,
                data: cadena ,
                success: function(r){
                    if(r==1){
                        $('#tabla').load('componentes-c/tablac.php');
                        // alertify.success("Eliminado con exito");
                        Swal.fire({
                            icon: 'success',
                            title: 'Eliminación exitosa',
                            text: "Cuadrilla eliminada",
        
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Continuar'
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        })
                        
                    }else{
                        // alertify.error("Error");
                        Swal.fire({
                            icon: 'error',
                            title: 'No se ha podido eliminar',
                            text: 'Ocurrió un error interno'
                        })
                    }
                }
            });
        }