<?php require_once '../sesiones/sesion.php';?>

<?php 
    require_once '../config/conexion.php';

    $modificar = $_GET['id_c'];

    $sql = "SELECT c.cdl_id, a.are_nombre, c.cdl_fecha_creacion ,a.are_id
    FROM cdl_cuadrilla c JOIN are_area a ON c.are_id = a.are_id
    WHERE cdl_id = '$modificar'";    

    $resultado = conexionbd()->query($sql);

    $datos = $resultado->fetch_array();

?>

<!DOCTYPE html> 
<html lang="en">
<head>    
    <title>Editar Cuadrilla</title>
    
    <?php   require_once '../extensiones/head.php';            
            require_once '../extensiones/nav_jefecuadrilla.php' ;
            require_once '../extensiones/scripts.php' ;

    ?>    
    <link rel="stylesheet" href="../../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <!-- <script src="js/funciones-proyecto.js"></script> --> 
    <!-- <script src="js/validar-proyecto.js"></script> -->
    <script src="js/validar-c.js"></script>
</head>
<body>
<div class="container mt-5 mb-5">                   
<h3 class="mb-4">Editar Cuadrilla</h3>
<form action="crud/editar-cuadrilla.php" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validarCuadrilla()">
                           <div class="row">   
                                                <div class="col-sm-12"> 
                                                     <input hidden =""type="text" name="id_cuadrilla" id="id_cuadrilla" value="<?php echo $modificar?>"> <!--CORREGIR -->
                                                </div>
               <!----------------------------------------------------------------------------------------------  -->
               <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_are = "SELECT * FROM are_area";
                                    $res = conexionbd()->query($sql_modificar_are);                                    
                                ?>
                                    <select class="form-control"name="are_id" id="are_id">
                                        <option value="" disabled="">Area de trabajo</option>                                                                       
                                        <?php 
                                            if($filas = $res->num_rows>0){
                                                while($are_id = $res->fetch_assoc()){ 
                                                    if($datos ['are_id'] == $are_id['are_id']){?>
                                                    <option selected="selected" value="<?php echo $are_id['are_id'];?>"><?php echo $are_id['are_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $are_id['are_id'];?>"> <?php echo $are_id['are_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>  
                             
            <!----------------------------------------------------------------------------------------------  -->
                           
                            <div class="col-sm-12">
                                 <div class="form-group ">                                     
                                     <input readonly type="date" class="form-control" id="cdl_fecha_creacion" name="cdl_fecha_creacion"  value="<?php echo $datos['cdl_fecha_creacion'];?>" >
                                </div>
                            </div>

                               </div>  

                               <div class="row">
                                <a href="#" onclick="confirmarC()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5" data-dismiss="modal">Cancelar</a>

                                <button id="editar_cuadrilla" name="editar_cuadrilla" style="margin: auto;" type="submit" class="btn btn-primary col-md-5" >Guardar Cambios</button>
                           </div>
                       </form>
                   
        </div>

        <script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="listado-cuadrilla.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>     
<!-- <script>
    $(document).ready(function () {  
        $('form').submit(function(e){
            var formData ={
                'pyt_nombre': $('#pyt_nombre').val(),
                'tpy_id': $('#tpy_id').val(),
                'pyt_fecha_inicio': $('#pyt_fecha_inicio').val(),
                'pyt_fecha_termino': $('#pyt_fecha_termino').val(),
                'pyt_presupuesto_inicial': $('#pyt_presupuesto_inicial').val(),
                'com_id': $('#com_id').val(),
                'epy_id': $('#epy_id').val()                
            };
            $.ajax({
                type: 'POST',
                url: 'agregar-proyecto.php',
                data: formData,
                dataType: 'json',
                encode: true
            })
            .done(function(data){
                if(!data.success){
                    Swal.fire({
                                    icon: 'error',
                                    title: 'Ha ocurrido un error',
                                    text: 'Debe completar todos los campos'
                                })
                }else{
                    Swal.fire({
                                    icon: 'success',
                                    title: 'Registro exitoso',
                                    text: "La marca se ha registrado con éxito",
                
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Continuar'
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "../area/lista-area.php";
                                    }
                                })
                }
            });
        });
    });
</script> -->



</body>
</html>