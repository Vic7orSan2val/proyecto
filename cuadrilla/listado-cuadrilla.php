<?php require_once '../sesiones/sesion.php';?>

<?php 
    require_once '../extensiones/head.php';
    require_once '../extensiones/nav_jefecuadrilla.php';
    require_once '../extensiones/scripts.php';
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Listado de cuadrillas</title>
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funcionesc.js"></script>
    <!-- <script src="js/validar-tipo.js"></script> -->
</head>
<body>
    <div class="container">
        <div class="menu">
            <div class="row">
                <div class="col-md-12">
                    <div class="titulo">
                        <h2>Cuadrillas de trabajo</h2>
                    </div>
                </div>

                <div class="col-md-12" id="tabla"></div>

            </div>
        </div>
    </div>



       <!--MODAL AGREGAR -->
<div class="modal fade" id="m_add_cuadrilla" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Cuadrilla</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container col-md-12">
                   <div class="formulario">
                       <form action="" method="POST" id="formulario_agregar" name="formulario_agregar" >
                           <div class="row">                                                      
                               <div class="col-sm-12">
                                <div class="form-group">

                                <?php 
                                    require_once '../config/conexion.php';
                                    $sql_add = "SELECT are_id, are_nombre FROM are_area";
                                    $res = conexionbd()->query($sql_add);
                                ?>
                                    <select class="form-control"name="are_id_a" id="are_id_a">
                                        <option value="0" disabled selected>Seleccione Area de trabajo</option>
                                        <?php 
                                            while($row = $res->fetch_assoc()){?>
                                                <option value="<?php echo $row['are_id']?>"> <?php echo $row['are_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div> 
                             
                            
                           
                           <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="date" class="form-control" id="cdl_fecha_a" name="cdl_fecha_a" placeholder="Ingrese nombre del area de trabajo"> 
                                    </div>
                               </div>  
                               </div>  
                       </form>
                   </div>
        </div>
      </div>
      <div class="modal-footer" >
        <a href="#" onclick="confirmarR()" id="cancelarcuadrilla" name="cancelarcuadrilla" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>
        <script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="listado-cuadrilla.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>

        <button id="addcuadrilla" name="addcuadrilla" style="margin: auto;" type="submit" class="btn btn-primary col-md-5">Registrar</button>
      </div>
    </div>
  </div>
</div>

 






</body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes-c/tablac.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#addcuadrilla').click(function(){  
                // alert('correcto');
                // are_id_a=$('#are_id_a').val();
                // cdl_fecha_a=$('#cdl_fecha_a').val();
                addCuadrilla();
           });


        //    $('#editarcuadrilla').click(function(){
        //          //alert('click');  

        //      modificararea();
        //    });


        


           


        });
    </script>
