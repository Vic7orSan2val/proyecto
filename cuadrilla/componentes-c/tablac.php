<?php 
    require_once '../../config/conexion.php';
    $sql = "SELECT c.cdl_id, a.are_nombre, c.cdl_fecha_creacion 
    FROM cdl_cuadrilla c JOIN are_area a ON c.are_id = a.are_id";
    $res = conexionbd()->query($sql);

    // $datos = $res->fetch_assoc();

    // echo $datos['cdl_fecha_creacion'];


    // Creación del array con los datos 
    $resultado2 = mysqli_query($conexion,$sql) or die (mysql_error ());

    $materiales = array();
    
    while( $rows = mysqli_fetch_assoc($resultado2) ) {
    
    $cuadrillas[] = $rows;
    
    }

    // Función que crea el excel

    if(isset($_POST["export_data"])) {

        if(empty($cuadrillas)) { ?>
            <script>
            alert('NO HAY DATOS PARA EXPORTAR')
            window.location = "../listado-cuadrilla.php"
            </script>
          <?php 
        }else{
           
            $filename = "Cuadrillas.xls";
            
            header("Content-Type: application/vnd.ms-excel");
            
            header("Content-Disposition:attachment; filename=".$filename);
            
            $mostrar_columnas = false;
    
            foreach($cuadrillas as $cuadrilla) {
    
                if(!$mostrar_columnas) {
    
                    echo implode("\t", array_keys($cuadrilla)) . "\n";
                    $mostrar_columnas = true;
                }
            echo implode("\t", array_values($cuadrilla)) . "\n";
            }
        }
    exit;
    }
?>

                   <br>
    <div class="container">                  
        <div class="table-responsive">
        <caption>
            <button type="submit" class="col-sm-4 btn btn-block btn-success" data-toggle="modal" data-target="#m_add_cuadrilla">Nuevo registro</button>
        </caption>
            <br>
            <table id="datatable" class="table table-bordered"> 
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Codigo Cuadrilla</th>
                        <th scope="col">Area de trabajo</th>
                        <th scope="col">Fecha de Creacion</th>
                        <th scope="col">Opciones</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                        if($res->num_rows>0){ 
                            while($cuadrilla = $res->fetch_assoc()){ 
                                
                            // $datos =    $cuadrilla['cdl_id']."||".
                            //             $cuadrilla['are_nombre']."||".
                            //             $cuadrilla['cdl_fecha_creacion'];
                                
                    ?>
                                <tr>
                                    <td> <?php echo $cuadrilla['cdl_id']?></td>
                                    <td> <?php echo $cuadrilla['are_nombre']?></td>
                                    <td> <?php echo $cuadrilla['cdl_fecha_creacion']?></td>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-6">                                
                                                <!-- <a href="#" class="btn btn-block btn-info" data-toggle="modal"
                                                data-target="#m_editar_cuadrilla" onclick="datosform('<?php //echo $datos?>')">Modificar</a> -->
                                                <a href="form-editar-cuadrilla.php?id_c=<?php echo $cuadrilla['cdl_id'];?>" class="btn btn-block btn-info">Modificar</a>

                                            </div>

                                            <div class="col-md-6">
                                                <!-- <button type="submit" onclick="confirmar(<?php //echo $cuadrilla['are_id'] ?>)" class="btn btn-block btn-danger">Eliminar</button> -->
                                                <button type="submit" class="btn btn-block btn-danger" onclick="confirmarEliminar(<?php echo $cuadrilla['cdl_id']?>)">Eliminar</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                    <?php
                        } 
                    }                                                        
                    ?>
                    
                </tbody>
            </table>                
        </div>
    </div>

    <div class="btn-group pull-right">
    <form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data'
            value="Export to excel" class="btn btn-info">Exportar a Excel</button>
    </form>
</div>         
                <!-- Botón que genera el excel -->
    
