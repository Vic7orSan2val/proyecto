<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once 'extensiones/head.php'; ?>
    <title>Inicio de Sesión</title>
    <link rel="shortcut icon" type="image/x-icon" href="imagenes/casco.ico">
</head>
<body background="imagenes/fondo.jpg">
<script src="validar.js"></script>

<div class="container mt-5">
<?php //require_once 'extensiones/nav.php'; ?>
        <div class="row justify-content-center mt-3 mr-1">
            <div class="col-md-5 p-4" style="background: #e6e6e6; border-radius: 3%;">
                <h2 class="text-left pt-4">Iniciar Sesión</h2><hr>
                <form id="form-login" action="validar.php" method="POST" onsubmit="return validar();">        
                    <div class="form-group mt-4">
                    <!-- <label for="user">Usuario</label> -->
                    <input type="text" class="form-control" name="tbj_rut" id="tbj_rut" placeholder="Ingrese su RUT" maxlength="12">          
                    </div>
                    <div class="form-group mt-4 mb-0">
                    <!-- <label for="clave">Contraseña</label> -->
                    <input type="password" class="form-control" name="tbj_clave" id="tbj_clave" placeholder="Ingrese su contraseña">          
                    </div>

                    <!-- <div class="form-group">
                      <label for="cargo"></label>
                      <select class="form-control" name="cargo" id="cargo">
                        <option selected disabled>Seleccione su cargo</option>
                        <option>admin</option>
                        <option>Jefe</option>
                      </select>
                    </div> -->

                    <div class="text-center  pt-4">                        
                        <input type="submit" class="btn btn-primary col-md-12" value="Ingresar" name="ingresar" id="ingresar">
                    </div>
<br>
                     
                </form>
            </div>
        </div>
    </div>  
    


    <?php require_once 'extensiones/scripts.php'?>
</body>
</html>

<script>
document.getElementById('tbj_rut').addEventListener('input', function(evt) {
  let value = this.value.replace(/\./g, '').replace('-', '');
  
  if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
    value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
  }
  else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
    value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, '$1.$2.$3-$4');
  }
  else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
    value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, '$1.$2.$3');
  }
  else if (value.match(/^(\d)(\d{0,2})$/)) {
    value = value.replace(/^(\d)(\d{0,2})$/, '$1.$2');
  }
  this.value = value;
});
</script>