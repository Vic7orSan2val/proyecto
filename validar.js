function validar(){
    var rut = document.getElementById('tbj_rut').value;
    var clave = document.getElementById('tbj_clave').value;
    var formato_rut = /^\d{1,2}\.\d{3}\.\d{3}[-][0-9kK]{1}$/;
    // var formato_rut = /^\d{1,2}\d{3}\d{3}[-][0-9kK]{1}$/;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO
    
        if(rut ==="" && clave ===""){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Complete todos los campos por favor'            
              })
            return false;
        }else if(rut ===""){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Debe ingresar un rut'            
              })
            return false; 
        }else if(!formato_rut.test(rut)){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Formato de rut no válido'           
              })
              return false; 
        }else if(rut.length > 12){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'El RUT es muy largo'            
              })
            return false; 
        }else if(clave ===""){
            Swal.fire({
                icon: 'error',
                title: 'Error...',
                text: 'Ingrese su contraseña'            
              })
            return false; 
        }
    

}