<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

<!-- alertify -->
<link rel="stylesheet" href="../librerias/alertifyjs/css/alertify.css">
<link rel="stylesheet" href="../librerias/alertifyjs/css/themes/default.css">
    

<!-- select2 -->
<link rel="stylesheet" href="../librerias/select2/css/select2.css">



