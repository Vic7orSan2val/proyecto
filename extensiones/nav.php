<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="../paginaprincipal/paginaprincipal.php">Constructora ARA</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <?php if(isset($_SESSION['tbj_rut'])){?>
        <li class="nav-item ">
          <a class="nav-link" href="../trabajadores/lista-trabajadores.php">Trabajadores <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="../cuadrilla/listado-cuadrilla.php">Cuadrillas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../materiales/lista-materiales.php">Materiales</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Gestion de Proyectos
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="../tipo-proyecto/listar-tipo-proyecto.php">Tipos de proyectos</a>
            <a class="dropdown-item" href="../proyecto/lista-proyectos.php">Proyectos </a>
            <a class="dropdown-item" href="../area/lista-area.php">Informacion sobre areas de trabajo</a>
            <a class="dropdown-item" href="../inventario/lista-inventario.php">Informacion inventarios</a>
          </div>
        </li>   

        <a class="nav-link" href="#" onclick="return confirmarClose()">Cerrar Sesión</a>

<script>
            function confirmarClose()
            {                        
                alertify.confirm('Cerrar Sesión',"¿Desea cerrar su sesión?",
                function(e){
                  if(e){
                    // alert('Modificacion Cancelada')
                    window.location="../cerrar_sesion.php"
                  }                                                 
                },
                function(){
                  alertify.message('Su sesión sigue activa');
                }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
            }
        </script>   

      </ul>                
      </div>
      <?php }?>
  </nav>
</div>
