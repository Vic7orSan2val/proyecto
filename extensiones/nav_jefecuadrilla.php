<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Constructora ARA</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="../trabajadores/lista-trabajadores.php">Trabajadores <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="../cuadrilla/listado-cuadrilla.php">Cuadrillas</a>
        </li>
        <li class="nav-item ">
          <a class="nav-link" href="../area/lista-area.php">Informacion sobre areas de trabajo</a>
        </li>
        
        <a class="nav-link" href="#" onclick="return confirmarClose()">Cerrar Sesión</a>

        <script>
                    function confirmarClose()
                    {                        
                        alertify.confirm('Cerrar Sesión',"¿Desea cerrar su sesión?",
                        function(e){
                          if(e){
                            // alert('Modificacion Cancelada')
                            window.location="../cerrar_sesion.php"
                          }                                                 
                        },
                        function(){
                          alertify.message('Su sesión sigue activa');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                    }
                </script> 
      </ul>            
      </div>
    
      
    
  </nav>
</div>