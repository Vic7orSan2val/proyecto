function agregardatos(inv_id, pyt_id){

    cadena = "inv_id=" + inv_id + "&pyt_id=" + pyt_id;

    $.ajax({
        type: "POST",
        url: "crud/agregar-inventario.php",
        data:  cadena,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes-i/tabla-i.php');
                //alert("Agregado con exito");
                Swal.fire({
                    icon: 'success',
                    title: 'Registro exitoso',
                    text: "La marca se ha registrado con éxito",

                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.href = "../inventario/lista-inventario.php";
                    }
                })
            }
            else{
               // alert("ERROR, NO AGREGADO");
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error',
                    text: 'Debe completar todos los campos'
                }) 
            }
        }
    });
}





function confirmarEliminar(inv_id){
    
    alertify.confirm('Elminar Tipo','¿Seguro que desea eliminar?', 
    function()
    {   eliminarTipo(inv_id)},
        // alertify.success('Si') } ,
    function(){
        alertify.message('Eliminación cancelada')});
    // alert('hola')
}

function eliminarTipo(inv_id){
cadena="inv_id=" + inv_id;

$.ajax({
    type: "POST",
    url: 'crud/eliminar-inventario.php' ,
    data: cadena ,
    success: function(r){
        if(r==1){
            $('#tabla').load('componentes-i/tabla-i.php');
            alertify.success("Eliminado con exito");
            
        }else{
            alertify.error("Error");
        }
    }
});
}