<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Inventarios</title>
    <?php require_once '../extensiones/head.php' ?>
    <?php require_once '../extensiones/nav_jefeinventario.php' ?>
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funciones-i.js"></script>
    <!-- <script src="js/validar-tipo.js"></script> -->
</head>
<body>
    <div class="container">
        <div class="menu">
            <div class="row">
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">                                                
                        <h4>Inventarios</h4><br>                        
                    </div>
                </div>
                <!-- Fin titulo -->

                <!-- BUSCADOR -->

                
                <!-- <div id="buscador" class="col-md-12"></div><br>
                <br> -->
                <br>
                <!--LLAMADA TABLA -->
                    <div class="col-md-12" id="tabla"></div>
                <!-- FIN LLAMADA TABLA -->



                                    <!--MODAL AGREGAR -->
<div class="modal fade" id="m_agregar_inv" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Inventario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container col-md-12">
                   <div class="formulario">
                       <form action="" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validar()">
                           <div class="row">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="inv_id" name="inv_id" placeholder="Ingrese numero de inventario"> 
                                    </div>
                               </div>                           
                               
                               
                               <div class="col-sm-12">
                                <div class="form-group">

                                <?php 
                                  require_once '../config/conexion.php';
                                    $sql_add = "SELECT pyt_id,pyt_nombre FROM pyt_proyecto";
                                    $res = conexionbd()->query($sql_add);
                                ?>
                                    <select class="form-control"name="pyt_id" id="pyt_id">
                                        <option value="0" disabled selected>Seleccione Proyecto</option>
                                        <?php 
                                            while($row = $res->fetch_assoc()){?>
                                                <option value="<?php echo $row['pyt_id']?>"> <?php echo $row['pyt_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div> 



                           </div>                          
                       </form>
                   </div>
        </div>
      </div>
      <div class="modal-footer" >
        <a href="#" onclick="confirmarR()" id="cancelartipo" name="cancelartipo" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>
        <script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="lista-inventario.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>


        <button id="addinv" name="addinv" style="margin: auto;" type="submit" class="btn btn-primary col-md-5">Registrar</button>
      </div>
    </div>
  </div>
</div>

 
<!-- EDITAR -->

           
            </div>
        </div>
    </div>

    
    <?php require_once '../extensiones/scripts.php' ?>
    

</body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes-i/tabla-i.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#addinv').click(function(){  
                // alert('correcto');
                inv_id=$('#inv_id').val();
                pyt_id=$('#pyt_id').val();
                agregardatos(inv_id, pyt_id)
           });      


        });
    </script>



