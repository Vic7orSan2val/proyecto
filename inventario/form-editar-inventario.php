<?php require_once '../sesiones/sesion.php';?>

<?php
    require_once '../config/conexion.php'; 
    
    $modificar = $_GET['id'];

    $sql = "SELECT inv.inv_id, p.pyt_nombre FROM inv_inventario inv JOIN pyt_proyecto p
    ON inv.pyt_id = p.pyt_id
    WHERE inv.inv_id = '$modificar'";
    $resultado = conexionbd()->query($sql);

    $datos = $resultado->fetch_array();

?>

<!DOCTYPE html>
<html lang="en">
<head>    
    <title>Editar inventario</title>
    
    <?php   require_once '../extensiones/head.php';            
            require_once '../extensiones/nav_jefeinventario.php' ;
            require_once '../extensiones/scripts.php' ;

    ?>    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <!-- <script src="js/funciones-proyecto.js"></script> --> 
    <!-- <script src="js/validar-proyecto.js"></script> -->
    <!-- <script src="validar.js"></script> -->
</head>
<body>
<div class="container mt-5 mb-5">                   
<h3 class="mb-4">Editar inventario</h3>
<form action="crud/editar-inventario.php" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validar()">
                           <div class="row">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="inv_id" name="inv_id" value="<?php echo $datos['inv_id']?>"> 
                                    </div>
                               </div>                           
                               
                               
                               <div class="col-sm-12">
                                <div class="form-group">

                                <?php 
                                  require_once '../config/conexion.php';
                                    $sql_add = "SELECT pyt_id,pyt_nombre FROM pyt_proyecto";
                                    $res = conexionbd()->query($sql_add);
                                ?>
                                    <select class="form-control"name="pyt_id" id="pyt_id">
                                        <option value="0" disabled selected>Seleccione Proyecto</option>
                                        <?php 
                                            while($row = $res->fetch_assoc()){?>
                                                <option value="<?php echo $row['pyt_id']?>"> <?php echo $row['pyt_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div> 
                           </div>  
                           
                           <div class="row">
                                <a href="#" onclick="confirmarC()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>
                                <script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="lista-inventario.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>

                                <button id="editar_area" name="editar_area" style="margin: auto;" type="submit" class="btn btn-primary col-md-5" >Guardar Cambios</button>
                           </div> 
                       </form>
</div>

     
<!-- <script>
    $(document).ready(function () {  
        $('form').submit(function(e){
            var formData ={
                'pyt_nombre': $('#pyt_nombre').val(),
                'tpy_id': $('#tpy_id').val(),
                'pyt_fecha_inicio': $('#pyt_fecha_inicio').val(),
                'pyt_fecha_termino': $('#pyt_fecha_termino').val(),
                'pyt_presupuesto_inicial': $('#pyt_presupuesto_inicial').val(),
                'com_id': $('#com_id').val(),
                'epy_id': $('#epy_id').val()                
            };
            $.ajax({
                type: 'POST',
                url: 'agregar-proyecto.php',
                data: formData,
                dataType: 'json',
                encode: true
            })
            .done(function(data){
                if(!data.success){
                    Swal.fire({
                                    icon: 'error',
                                    title: 'Ha ocurrido un error',
                                    text: 'Debe completar todos los campos'
                                })
                }else{
                    Swal.fire({
                                    icon: 'success',
                                    title: 'Registro exitoso',
                                    text: "La marca se ha registrado con éxito",
                
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Continuar'
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "../area/lista-area.php";
                                    }
                                })
                }
            });
        });
    });
</script> -->



</body>
</html>