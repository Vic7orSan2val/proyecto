function validarTrabajador(){
    var rut = document.getElementById('rut_e').value;  
    var nombre = document.getElementById('nombre_e').value;  
    var apellido = document.getElementById('apellido_e').value;  
    var fecha_nac = document.getElementById('fecha_nac_e').value;  
    var correo = document.getElementById('correo_e').value;  
    var comuna = document.getElementById('com_id').value;  
    var direccion = document.getElementById('direccion_e').value;  
    var telefono = document.getElementById('telefono_e').value; 
    var estado = document.getElementById('etj_id').value; 
    var cuadrilla = document.getElementById('cdl_id').value; 
    var rol = document.getElementById('rol_id').value; 
    var clave = document.getElementById('clave_e').value;

    var formato_rut = /^\d{1,2}\.\d{3}\.\d{3}[-][0-9kK]{1}$/;
    var expresion_correo = /\w+@\w+\.+[a-z]/; //expresion correo
    // var expresion_rut -> HAY QUE CREARLA PARA VALIDAR RUT
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var expresioncn = /[a-zA-ZÀ-ÿ]/;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO

    if(rut ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete el RUT'           
          })
          return false; 
    }
    // else if(novale.test(rut)){
    //     alert('no pueden existir espacio s en blanco al principio');
    //     return false;
    // }
    else if(!formato_rut.test(rut)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Formato de RUT no válido'           
          })
          return false; 
    }
    else if(nombre === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete el nombre'           
          })
          return false; 
    }else if(novale.test(nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Nombre no puede comenzar con espacio en blanco'           
          })
          return false; 
    }else if(!expresion.test(nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Nombre solo permite letras'           
          })
          return false; 
    }else if(apellido === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete el apellido'           
          })
          return false; 
    }else if(novale.test(apellido)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Apellido no puede comenzar con espacio en blanco'           
          })
          return false; 
    }else if(!expresion.test(apellido)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Apellido solo permite letras'           
          })
          return false; 
    }else if(fecha_nac ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese una fecha de nacimiento'           
          })
          return false; 
    }else if(correo ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese un correo electronico'           
          })
          return false; 
    }else if(novale.test(correo)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Correo no puede comenzar con espacio en blanco'           
          })
          return false; 
    }else if(!expresion_correo.test(correo)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Formato de correo es incorrecto: mrn0706@gmail.com'           
          })
          return false; 
    }else if(comuna ==="0" || comuna===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una comuna por favor'           
          })
          return false; 
    }else if(direccion===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese una direccion por favor'           
          })
          return false; 
    }else if(novale.test(direccion)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'La direccion no puede comenzar con espacio en blanco'           
          })
          return false; 
    }else if(!expresioncn.test(direccion)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Direccion solo permite letras y numeros'           
          })
          return false; 
    }else if(telefono ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese un telefono por favor'           
          })
          return false; 
    }else if(novale.test(telefono)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Telefono no puede comenzar con un espacio en blanco'           
          })
          return false; 
    }else if(isNaN(telefono)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Telefono solo permite numeros'           
          })
          return false; 
    }else if(estado==="0" || estado === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un estado para el trabajador por favor'           
          })
          return false; 
    }else if(cuadrilla ==="0" || cuadrilla ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una cuadrilla por favor'           
          })
          return false; 
    }else if(rol ==="0" || rol=== ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un rol por favor'           
          })
          return false; 
    }else if(clave ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese una contraseña por favor'           
          })
          return false; 
    }else{
        Swal.fire({
            icon: 'success',
            title: 'Modificacion exitosa...',
            text: 'La modificacion se ha realizado con exito'           
          })
         
    }

}