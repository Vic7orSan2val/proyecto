function agregartrabajador(){   
    var rut = document.getElementById('rut').value;  
    var nombre = document.getElementById('nombre').value;  
    var apellido = document.getElementById('apellido').value;  
    var fecha_nac = document.getElementById('fecha_nac').value;  
    var correo = document.getElementById('correo').value;  
    var comuna = document.getElementById('comuna_id').value;  
    var direccion = document.getElementById('direccion').value;  
    var telefono = document.getElementById('telefono').value; 
    var estado = document.getElementById('estado').value; 
    var cuadrilla = document.getElementById('cuadrilla').value; 
    var rol = document.getElementById('rol').value; 
    var clave = document.getElementById('clave').value;

    var formato_rut = /^\d{1,2}\.\d{3}\.\d{3}[-][0-9kK]{1}$/;
    var expresion_correo = /\w+@\w+\.+[a-z]/; //expresion correo
    // var expresion_rut -> HAY QUE CREARLA PARA VALIDAR RUT
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var expresioncn = /[a-zA-ZÀ-ÿ]/;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO

    if(rut ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete el campo RUT'           
          })
          return false; 
    }
    // else if(novale.test(rut)){
    //     alert('no pueden existir espacios en blanco al principio');
    //     return false;
    // }
    else if(!formato_rut.test(rut)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Formato de rut no válido'           
          })
          return false; 
    }
    else if(nombre === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Nombre no puede estar vacio'           
          })
          return false; 
    }else if(novale.test(nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Nombre no puede tener espacio vacio al inicio'           
          })
          return false; 
    }else if(!expresion.test(nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Nombre solo permite letras'           
          })
          return false; 
    }else if(apellido === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Apellido no puede estar vacio'           
          })
          return false; 
    }else if(novale.test(apellido)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Apellido no puede comenzar con un espacio'           
          })
          return false; 
    }else if(!expresion.test(apellido)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Apellido solo permite letras'           
          })
          return false; 
    }else if(fecha_nac ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'La fecha no puede estar vacia'           
          })
          return false; 
    }else if(correo ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Correo no puede estar vacio'           
          })
          return false; 
    }else if(novale.test(correo)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Correo no puede comenzar con espacio en blanco'           
          })
          return false; 
    }else if(!expresion_correo.test(correo)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Formato de correo invalido, revise por favor'           
          })
          return false; 
    }else if(comuna ==="0" || comuna===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una comuna por favor'           
          })
          return false; 
    }else if(direccion===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Direccion no puede estar vacia'           
          })
          return false; 
    }else if(novale.test(direccion)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Direccion no puede comenzar con espacio en blanco'           
          })
          return false; 
    }else if(!expresioncn.test(direccion)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Direccion solo puede contener letras'           
          })
          return false; 
    }else if(telefono ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Telefono no puede estar vacio'           
          })
          return false; 
    }else if(novale.test(telefono)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Telefono no puede comenzar con espacios en blanco'           
          })
          return false; 
    }else if(isNaN(telefono)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Telefono solo permite numeros'           
          })
          return false; 
    }else if(estado==="0" || estado === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione estado para trabajador'           
          })
          return false; 
    }else if(cuadrilla ==="0" || cuadrilla ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una cuadrilla por favor'           
          })
          return false; 
    }else if(rol ==="0" || rol=== ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un rol por favor'           
          })
          return false; 
    }else if(clave ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese una contraseña por favor'           
          })
          return false; 
    }
    else{
    // alert('biens');
    cadena =    "rut=" + rut + 
                "&nombre=" + nombre + 
                "&apellido=" + apellido + 
                "&fecha_nac=" + fecha_nac+ 
                "&correo=" + correo+ 
                "&comuna=" + comuna+ 
                "&direccion=" + direccion+ 
                "&telefono=" + telefono+ 
                "&estado=" + estado+ 
                "&cuadrilla=" + cuadrilla+
                "&rol=" + rol +
                "&clave=" + clave;

    $.ajax({
        type: "POST",
        url: "crud/agregar-trabajador.php",
        data:  cadena,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes-t/tabla-t.php');
                //alert("Agregado con exito");
                Swal.fire({
                    icon: 'success', 
                    title: 'Registro exitoso',
                    text: "La marca se ha registrado con éxito",

                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.href = "../trabajadores/lista-trabajadores.php";
                    }
                })
            }
            else{
               // alert("ERROR, NO AGREGADO");
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error',
                    text: 'Debe completar todos los campos'
                    
                })
            }
        }
    });
}
}
// function datosform(datos){
//     d = datos.split('||');
//     $('#rut_e').val(d[0]),
//     $('#nombre_e').val(d[1]),
//     $('#apellido_e').val(d[2]),
//     $('#fecha_nac_e').val(d[3]),
//     $('#correo_e').val(d[4]),
//     $('#comuna_id_e').val(d[5]),
//     $('#direccion_e').val(d[6]),
//     $('#telefono_e').val(d[7]),
//     $('#estado_e').val(d[8]),
//     $('#cuadrilla_e').val(d[9]),
//     $('#rol_e').val(d[10]),
//     $('#clave_e').val(d[11])
// }
// function modificartrabajador(){
      
//     rut=$('#rut_e').val();
//     nombre=$('#nombre_e').val();
//     apellido=$('#apellido_e').val();
//     fecha_nac=$('#fecha_nac_e').val();
//     correo= $('#correo_e').val();
//     comuna=$('#comuna_id_e').val();
//     direccion=$('#direccion_e').val();
//     telefono= $('#telefono_e').val();
//     estado=$('#estado_e').val();
//     cuadrilla=$('#cuadrilla_e').val();
//     rol=$('#rol_e').val();
//     clave=$('#clave_e').val();

//     cadena =    "rut=" + rut + 
//                 "&nombre=" + nombre + 
//                 "&apellido=" + apellido + 
//                 "&fecha_nac=" + fecha_nac+ 
//                 "&correo=" + correo+ 
//                 "&comuna=" + comuna+ 
//                 "&direccion=" + direccion+ 
//                 "&telefono=" + telefono+ 
//                 "&estado=" + estado+ 
//                 "&cuadrilla=" + cuadrilla+
//                 "&rol=" + rol +
//                 "&clave=" + clave;
                
//                 $.ajax({
//                     type: "POST",
//                     url: "crud/editar-trabajador.php",
//                     data:  cadena,
//                     success: function(r){
//                         if(r==1){
//                             $('#tabla').load('componentes-t/tabla-t.php');
//                             //alert("Agregado con exito");
//                             Swal.fire({
//                                 icon: 'success',
//                                 title: 'Registro exitoso',
//                                 text: "La marca se ha registrado con éxito",
            
//                                 showCancelButton: false,
//                                 confirmButtonColor: '#3085d6',
//                                 cancelButtonColor: '#d33',
//                                 confirmButtonText: 'Continuar'
//                             }).then((result) => {
//                                 if (result.value) {
//                                     location.href = "../trabajadores/lista-trabajadores.php";
//                                 }
//                             })
//                         }
//                         else{
//                            // alert("ERROR, NO AGREGADO");
//                             Swal.fire({
//                                 icon: 'error',
//                                 title: 'Ha ocurrido un error',
//                                 text: 'Debe completar todos los campos'
//                             })
//                         }
//                     }
//                 });
                 
// }

 
// // ELIMINAR
// function confirmar(tbj_rut){
    
//     // alertify.confirm('Eliminar Proyecto','¿Seguro que desea eliminar?', 
//     // function()
//     // {   eliminarProyecto(tbj_rut)},
//     //     // alertify.success('Si') } ,
//     // function(){
//     //     alertify.message('Eliminación cancelada')}); 
//     // alert('hola')

//     const swalWithBootstrapButtons = Swal.mixin({
//         customClass: {
//             confirmButton: 'btn btn-success',
//             cancelButton: 'btn btn-danger'
//         },
//         buttonsStyling: true
//         })

//         swalWithBootstrapButtons.fire({
//         title: '¿Está seguro que desea eliminar el del trabajador?',
//         text: "No podrá recuperar el registro guardado!",
//         icon: 'warning',
//         showCancelButton: true,
//         confirmButtonText: '¡Sí, eliminar!',
//         cancelButtonText: '¡No, cancelar!',
//         reverseButtons: false,
//         confirmButtonColor: '#3085d6',
//         cancelButtonColor: '#d33',
//         }).then((result) => {
//         if (result.value) {
//             eliminarTrabajador(tbj_rut)
//         } else if (
//             /* Read more about handling dismissals below */
//             result.dismiss === Swal.DismissReason.cancel
//         ) {
//             swalWithBootstrapButtons.fire(
//             'Cancelado',
//             'El trabajador no se eliminará',
//             'error'
//             )
//         }
//         })

// }

// function eliminarTrabajador(tbj_rut){
// cadena="tbj_rut=" + tbj_rut;

//     $.ajax({
//         type: "POST",
//         url: 'crud/eliminar-trabajador.php' ,
//         data: cadena ,
//         success: function(r){
//             if(r==1){
//                 $('#tabla').load('componentes-t/tabla-t.php');
//                 alertify.success("Eliminado con exito");
                
//             }else{
//                 alertify.error("Error");
//             }
//         }
//     });
// }