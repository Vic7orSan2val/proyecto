<?php
require_once("../config/db.php"); //Contiene las variables de configuracion para conectar a la base de datos
require_once("../config/conexion.php"); //Contiene funcion que conecta a la base de datos

$rut_trabajador = $_GET['rut'];
$consulta = "SELECT * FROM trabajadores WHERE rut = '$rut_trabajador'";

$result = $con->query($consulta);
$result = $result->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <?php require '../extensiones/head.php' ?>
    <title>Editar trabajador</title>
    <link rel="shortcut icon" type="image/x-icon" href="../img/trabajadores.png">
</head>

<body style="background:linear-gradient(20deg, LightGray, Gainsboro);";>
    <?php require '../extensiones/nav-trabajadores.php' ?>
<br><br>
    <div class="contenedor">        

        <div class="form">
            <h4>Formulario de Modificacion</h4>
            <form action="editar-trabajador.php" method="POST">
                <p>
                    <label for="nombre">Nombres</label>
                    <input type="text" placeholder="Ingrese su Nombre" id="nombre" name="nombre" value="<?php echo $result['nombre'] ?>" required>
                </p>
                <p>
                    <label for="apellido">Apellidos</label>
                    <input type="text" placeholder="Ingrese su Apellido" id="apellido" name="apellido" value="<?php echo $result['apellido'] ?>" required>
                </p>
                <p>
                    <label for="rut">Rut</label>
                    <input type="text" placeholder="12.123.123-3" id="rut" name="rut" value="<?php echo $result['rut'] ?>" readonly>
                </p>
                <p>
                    <label for="correo">Correo</label>
                    <input type="email" placeholder="Ingrese su Correo" id="correo" name="correo" value="<?php echo $result['correo'] ?>" required>
                </p>
                <p>
                    <label for="telefono">Telefono</label>
                    <input type="tel" placeholder="Ingrese su N°contacto" id="telefono" name="telefono" value="<?php echo $result['telefono'] ?>" required>
                </p>
                <p>
                    <label for="ciudad">Ciudad</label>
                    <input type="text" placeholder="Ingrese su Ciudad" id="ciudad" name="ciudad" value="<?php echo $result['ciudad'] ?>" required>
                </p>
                <p>
                    <label for="afiliacion_afp">Afiliación Afp</label>
                    <input type="text" placeholder="Ingrese su Afiliación Afp" id="afiliacion_afp" name="afiliacion_afp" value="<?php echo $result['afiliacion_afp'] ?>" required>
                </p>
                <p>
                    <label for="salud">Sistema de Salud</label>
                    <input type="text" placeholder="Ingrese su Sistema de Salud" id="salud" name="salud" value="<?php echo $result['salud'] ?>" required>
                </p>

                <button type="submit" class="button2">Modificar Trabajador</i></button>
            </form>

            <div class="row">
                <div class="col-md-12">
                    <?php
                    if (!empty($_GET['error'])) {
                        $respuesta = $_GET['error'];
                        $contenido = $_GET['contenido']; ?>
                        <?php if ($respuesta == 'vacio') { ?>
                            <div class="col-md-12">
                                <div class="alert alert-success" role="alert">
                                    <?php echo $contenido ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

        </div>



    </div>

    <?php require '../extensiones/scripts.php' ?>
</body>

</html>