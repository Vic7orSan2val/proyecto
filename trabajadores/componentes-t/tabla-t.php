<?php 
    include '../../config/conexion.php';
   
 
        $sql = "SELECT t.tbj_rut, t.tbj_nombre, t.tbj_apellido, t.tbj_fecha_nac, t.tbj_correo, t.tbj_direccion,
        t.tbj_telefono, c.com_nombre, r.rol_nombre, etj.etj_nombre, cdl.cdl_id, rgn.rgn_nombre
        FROM tbj_trabajador t 
        INNER JOIN com_comuna c ON t.com_id = c.com_id
        INNER JOIN etj_estado_trabajador etj ON t.etj_id = etj.etj_id
        INNER JOIN rol r ON t.rol_id = r.rol_id 
        INNER JOIN cdl_cuadrilla cdl ON t.cdl_id = cdl.cdl_id
        INNER JOIN rgn_region rgn ON c.rgn_id = rgn.rgn_id";

        $resultado = conexionbd()->query($sql);

        // Creación del array con los datos 
        $resultado2 = mysqli_query($conexion,$sql) or die (mysql_error ());

        $trabajadores = array();
        
        while( $rows = mysqli_fetch_assoc($resultado2) ) {
        
        $trabajadores[] = $rows;
        
        }

        // Función que crea el excel

        if(isset($_POST["export_data"])) {

            if(empty($trabajadores)) { ?>
                <script>
                    alert('NO EXISTEN DATOS PARA EXPORTAR')
                    window.location = "../lista-trabajadores.php"
                </script>
                <?php
            }else{
                
                $filename = "Trabajadores.xls";
                
                header("Content-Type: application/vnd.ms-excel");
                
                header("Content-Disposition:attachment; filename=".$filename);
                
                $mostrar_columnas = false;
        
                foreach($trabajadores as $trabajador) {
        
                    if(!$mostrar_columnas) {
        
                        echo implode("\t", array_keys($trabajador)) . "\n";
                        $mostrar_columnas = true;
                    }
                echo implode("\t", array_values($trabajador)) . "\n";
                }
            }
        exit;
        }
    
?>

 <!-- Inicio tabla -->
<div class="row">
    <div class="table-responsive">
        <caption>
        <!-- <a href="#" class="btn btn-block btn-success">Agregar</a> -->
        <button class="col-sm-4 btn btn-block btn-success"data-toggle="modal" data-target="#m_agregar_trabajador">Agregar Trabajador</button>
        </caption><br>
        <table class="table table-bordered" id="datatable">
            <thead class="thead-dark">
                <tr>
                    <th>R.U.T</th>
                    <th>Nombre</th>
                    <th>Apellido</th>                
                    <th>Fecha Nacimiento</th>
                    <th>Correo</th>
                    <!-- <th>Region</th> -->
                    <th>Comuna</th>
                    <th>Direccion</th>
                    <th>Telefono</th>                                        
                    <th>Estado Trabajador</th>
                    <th>Cuadrilla</th>
                    <th>Rol</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($resultado->num_rows>0):
                    while($trabajador = $resultado->fetch_assoc()):
     
                ?>
                <tr>
                    
                    <td><?php echo $trabajador['tbj_rut'] ?></td>
                    <td><?php echo $trabajador['tbj_nombre'] ?></td>
                    <td><?php echo $trabajador['tbj_apellido']?></td>
                    <td><?php echo $trabajador['tbj_fecha_nac']?></td>                    
                    <td><?php echo $trabajador['tbj_correo']?></td>
                    <!-- <td><?php //echo $trabajador['rgn_nombre']?></td> -->
                    <td><?php echo $trabajador['com_nombre']?></td>
                    <td><?php echo $trabajador['tbj_direccion']?></td>
                    <td><?php echo $trabajador['tbj_telefono']?></td> 
                    <td><?php echo $trabajador['etj_nombre']?></td>
                    <td><?php echo $trabajador['cdl_id']?></td>                    
                    <td><?php echo $trabajador['rol_nombre']?></td>                    
                    
                   
                    <td>
                        <div class="row">
                            <div class="col-sm-6">                                
                                <!-- <a href="#" class="btn btn-block btn-info" data-toggle="modal"
                                 data-target="#m_editar_trabajador" onclick="datosform('<?php //echo $datos?>')">Modificar</a> -->
                                 <a class="btn btn-block btn-info" href="form-editar-trabajadores.php?rut=<?php echo $trabajador['tbj_rut']?>">Modificar</a>
                            </div>

                            <div class="col-sm-6">
                                <!-- <button type="submit" onclick="conf(<?php $trabajador['tbj_nombre']?>);" class="btn btn-block btn-danger">Eliminar</button> -->
                                <!-- <button type="submit" class="btn btn-block btn-danger" onclick="confirmar(<?php // echo $trabajador['tbj_rut']?>)">Eliminar</button> -->
                                <!-- <a class="btn btn-block btn-danger" href="crud/eliminar-trabajador.php?rut=<?php //echo $trabajador['tbj_rut']?>">Editar**</a> -->
                                <a href="crud/eliminar-trabajador.php?tbj_rut=<?php echo $trabajador['tbj_rut']?>" class="btn btn-block btn-danger" onclick="return confirmarEliminarTrabajador();">Eliminar</a>                                                                
                                <script>
                                function confirmarEliminarTrabajador(){
                                    let r = confirm('¿Está seguro de eliminar el registro?')
                                    if(r){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                }
                                </script>
                            
                            </div>
                        </div>
                    </td>
                </tr>
                <?php 
                    endwhile;
                endif;                
                ?>
            </tbody>
        </table>
    </div>
</div>

               <!-- Fin tabla -->

<div class="btn-group pull-right">
    <form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data'
            value="Export to excel" class="btn btn-info">Exportar a Excel</button>
    </form>
</div>         
                <!-- Botón que genera el excel -->
