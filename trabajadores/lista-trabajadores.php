<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Administracion de trabajadores</title>
    <?php
        require_once '../config/conexion.php'; 
        require_once '../extensiones/head.php'; 
        require_once '../extensiones/nav_jefecuadrilla.php' ;
    ?>
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funcionest.js"></script>
    <!-- <script src="js/validar-proyecto.js"></script> -->
</head>
<body>
    <div class="ml-5 mr-5">
        <div class="menu">
            <div class="row">
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">                                                
                        <h4>Trabajadores</h4><br>                        
                    </div>
                </div>
                <!-- Fin titulo -->

                <!-- BUSCADOR -->
                
                <!-- <div id="buscador" class="col-md-12"></div><br>
                <br> -->
                <br>
                <!--LLAMADA TABLA -->
                    <div class="col-md-12" id="tabla"></div>
                <!-- FIN LLAMADA TABLA --> 

 

                                    <!--MODAL AGREGAR -->
<div class="modal fade" id="m_agregar_trabajador" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Registrar Trabajador</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      <div class="container ">                   
                       <form action="" method="POST" id="formulario_agregar" name="formulario_agregar">
                           <div class="row">
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="rut" id="rut" placeholder="Ingrese R.U.T del trabajador | Ej: 12.088.123-7" maxlength="12">
                                   </div>
                               </div>

                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="nombre" id="nombre" placeholder="Ingrese nombre del trabajador">
                                   </div>
                               </div>
 
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="apellido" id="apellido" placeholder="Ingrese apellido del trabajador">
                                   </div>
                               </div>
                               
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="date" name="fecha_nac" id="fecha_nac">
                                   </div>
                               </div>

                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="correo" id="correo" placeholder="Ingrese correo del trabajador">
                                   </div>
                               </div>

                            <!--SELECT DINAMICO REGIONES-->
                            <div class="form-group col-md-6">
                                <label for="rgn_id">Seleccione la region</label>
                                <select id="rgn_id" name="rgn_id" class="form-control">                                
                                </select>                                           
                            </div> 
                            <!-- SELECT COMUNAS-->
                            <div class="form-group col-md-6">
                                <label for="comuna_id">Seleccione la comuna</label>
                                    <select id="comuna_id" name="comuna_id" class="form-control">   
                                    <option value='0'>Elige una comuna</option>             
                                    </select>                                           
                            </div>
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="direccion" id="direccion" placeholder="Ingrese direccion del trabajador">
                                   </div>
                               </div>
                               <div class="col-sm-12">
                                   <div class="form-group">                                       
                                    <input class="form-control" type="text" name="telefono" id="telefono" placeholder="Ingrese telefono del trabajador">
                                   </div>
                               </div>
                               <div class="col-sm-12">
                                   <div class="form-group">
                                   <?php 
                                        $consulta = "SELECT * FROM etj_estado_trabajador";
                                        $estado = conexionbd()->query($consulta);
                                       ?>
                                    <select class="form-control" name="estado" id="estado">
                                        <option value="0" disabled selected>Seleccione estado</option>
                                        <?php 
                                            while($row_estado = $estado->fetch_assoc()){ ?>
                                                <option value="<?php echo $row_estado['etj_id']?>"><?php echo $row_estado['etj_nombre']?></option>
                                          <?php  }
                                        ?>
                                    </select>
                                   </div>
                               </div>
                               <div class="col-sm-12">
                                <div class="form-group">

                                <?php 
                                    require_once '../config/conexion.php';
                                    $sql_add = "SELECT cdl_id FROM cdl_cuadrilla";
                                    $resc = conexionbd()->query($sql_add);
                                ?>
                                    <select class="form-control"name="cuadrilla" id="cuadrilla">
                                        <option value="0" disabled selected>Seleccione Cuadrilla</option>
                                        <?php 
                                            while($rowc = $resc->fetch_assoc()){?>
                                                <option value="<?php echo $rowc['cdl_id']?>"> <?php echo $rowc['cdl_id']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php 
                                        $roles = "SELECT * FROM rol";
                                        $result = conexionbd()->query($roles);
                                    ?>
                                    <select class="form-control"name="rol" id="rol">
                                        <option value="0" disabled selected>Seleccione Rol</option>
                                        <?php 
                                            while($rowrol = $result->fetch_assoc()){?>
                                                <option value="<?php echo $rowrol['rol_id']?>"> <?php echo $rowrol['rol_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="password" id="clave" name="clave" placeholder="Ingrese su contraseña">
                                </div>
                            </div>
                               
                           </div>     
                           <div class="row">
                                <a href="#" onclick="confirmarR()" id="cancelartrabajador" name="cancelartrabajador" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>

                                <script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="lista-trabajadores.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>

                                <button id="agregartrabajador" name="agregartrabajador" style="margin: auto;" type="button" class="btn btn-primary col-md-5" >Registrar</button>
                           </div>
                          
                       </form>
                   
        </div>
      </div>
                <div class="modal-footer" >
      
                </div>
    </div>
  </div>
</div>




           
            </div>
        </div>
    </div>

    
    <?php require_once '../extensiones/scripts.php' ?>
    

</body>
</html>


<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes-t/tabla-t.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#agregartrabajador').click(function(){  
                // alert('correcto');
                // pyt_nombre=$('#pyt_nombre').val();
            //    rut=$('#rut').val();
            //    nombre=$('#nombre').val();
            //    apellido=$('#apellido').val();
            //    fecha_nac=$('#fecha_nac').val();
            //    correo=$('#correo').val();
            //    comuna=$('#comuna_id').val();
            //    direccion=$('#direccion').val();
            //    telefono=$('#telefono').val();
            //    estado=$('#estado').val();
            //    cuadrilla=$('#cuadrilla').val();
            //    rol=$('#rol').val();
            //    clave=$('#clave').val();
                
                agregartrabajador();
           });


        //    $('#editar_trabajador').click(function(){
        //          //alert('click');    
        //     modificartrabajador();
        //    });




           


        });
    </script>

<script>
//AJAX PARA SELECCIONAR REGIONES Y COMUNAS
$(document).ready(function(){
   // alert('GOLA')
    $.ajax({
        type: 'POST',
        url: 'select-region.php'
    })
    .done(function(resp){
        //alert('TODO BIEN')
        $('#rgn_id').html(resp)
    })
    .fail(function(){
        //alert('HUBI UN ERROOR CONLAS REGIONAS')
    })
    
///AJAX COMUNAS
    $('#rgn_id').on('change', function(){
        var id = $('#rgn_id').val()
        //alert(id)
        $.ajax({
            type: 'POST',
            url: 'select-comuna.php',
            data: {'id': id}
        })
        .done(function(resp){
           // alert('TODO BIEN')
            $('#comuna_id').html(resp)
        })
        .fail(function(){
            alert('HUBI UN ERROOR CONLAS comuNAS')
        })
    })       
})
  </script>

<script>
document.getElementById('rut').addEventListener('input', function(evt) {
  let value = this.value.replace(/\./g, '').replace('-', '');
  
  if (value.match(/^(\d{2})(\d{3}){2}(\w{1})$/)) {
    value = value.replace(/^(\d{2})(\d{3})(\d{3})(\w{1})$/, '$1.$2.$3-$4');
  }
  else if (value.match(/^(\d)(\d{3}){2}(\w{0,1})$/)) {
    value = value.replace(/^(\d)(\d{3})(\d{3})(\w{0,1})$/, '$1.$2.$3-$4');
  }
  else if (value.match(/^(\d)(\d{3})(\d{0,2})$/)) {
    value = value.replace(/^(\d)(\d{3})(\d{0,2})$/, '$1.$2.$3');
  }
  else if (value.match(/^(\d)(\d{0,2})$/)) {
    value = value.replace(/^(\d)(\d{0,2})$/, '$1.$2');
  }
  this.value = value;
});
</script>