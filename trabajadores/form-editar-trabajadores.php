<?php require_once '../sesiones/sesion.php';?>

<?php
    require_once '../config/conexion.php'; 
    
    $modificar = $_GET['rut'];

    $sql = "SELECT t.tbj_rut, t.tbj_nombre, t.tbj_apellido, t.tbj_fecha_nac, t.tbj_correo, t.tbj_direccion,
    t.tbj_telefono, c.com_nombre, r.rol_nombre, etj.etj_nombre, cdl.cdl_id, t.tbj_clave,
    c.com_id, etj.etj_id, cdl.cdl_id, r.rol_id
    FROM tbj_trabajador t LEFT JOIN com_comuna c ON t.com_id = c.com_id
    LEFT JOIN etj_estado_trabajador etj ON t.etj_id = etj.etj_id
    LEFT JOIN rol r ON t.rol_id = r.rol_id 
    LEFT JOIN cdl_cuadrilla cdl ON t.cdl_id = cdl.cdl_id
    WHERE t.tbj_rut = '$modificar'"; 

    $resultado = conexionbd()->query($sql);

    $datos = $resultado->fetch_array();
?>

<!DOCTYPE html>
<html lang="en">
<head>     
    <title>Editar trabajador</title>
    
    <?php   require_once '../extensiones/head.php';            
            require_once '../extensiones/nav_jefecuadrilla.php' ;
            require_once '../extensiones/scripts.php' ;

    ?>    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <!-- <script src="js/funciones-proyecto.js"></script> --> 
    <!-- <script src="js/validar-proyecto.js"></script> -->
    <script src="js/validar-t.js"></script>
</head>
<body>
<div class="container mt-5 mb-5">                   
<h3 class="mb-4">Editar Trabajador</h3>
<form action="crud/editar-trabajador.php" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validarTrabajador()">
                           <div class="row">
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" readonly name="rut_e" id="rut_e" value="<?php echo $datos['tbj_rut'];?>">
                                   </div>
                               </div>

                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="nombre_e" id="nombre_e" value="<?php echo $datos['tbj_nombre'];?>">
                                   </div>
                               </div>

                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="apellido_e" id="apellido_e"  value="<?php echo $datos['tbj_apellido'];?>">
                                   </div>
                               </div>
                               
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="date" name="fecha_nac_e" id="fecha_nac_e"  value="<?php echo $datos['tbj_fecha_nac'];?>">
                                   </div>
                               </div>

                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="correo_e" id="correo_e"  value="<?php echo $datos['tbj_correo'];?>">
                                   </div>
                               </div>

                               
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_com = "SELECT com_id, com_nombre FROM com_comuna";
                                    $res_com = conexionbd()->query($sql_modificar_com);                                    
                                ?>
                                    <select class="form-control"name="com_id" id="com_id">
                                        <option value="" disabled>Comuna</option>                                                                       
                                        <?php 
                                            if($res_com->num_rows>0){
                                                while($com_id = $res_com->fetch_array()){ 
                                                    if($datos['com_id'] == $com_id['com_id']){?>
                                                    <option selected="selected" value="<?php echo $com_id['com_id'];?>"><?php echo $com_id['com_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $com_id['com_id'];?>"> <?php echo $com_id['com_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div> 
                            
                               <div class="col-sm-12">
                                   <div class="form-group">
                                    <input class="form-control" type="text" name="direccion_e" id="direccion_e" value="<?php echo $datos['tbj_direccion'];?>">
                                   </div>
                               </div>
                               <div class="col-sm-12">
                                   <div class="form-group">                                       
                                    <input class="form-control" type="text" name="telefono_e" id="telefono_e" value="<?php echo $datos['tbj_telefono'];?>">
                                   </div>
                               </div>
                               
                               <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_etj = "SELECT * FROM etj_estado_trabajador";
                                    $res_etj = conexionbd()->query($sql_modificar_etj);                                    
                                ?>
                                    <select class="form-control"name="etj_id" id="etj_id">
                                        <option value="" disabled="">Estado trabajador</option>                                                                       
                                        <?php 
                                            if($filas = $res_etj->num_rows>0){
                                                while($etj_id = $res_etj->fetch_assoc()){ 
                                                    if($datos ['etj_id'] == $etj_id['etj_id']){?>
                                                    <option selected="selected" value="<?php echo $etj_id['etj_id'];?>"><?php echo $etj_id['etj_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $etj_id['etj_id'];?>"> <?php echo $etj_id['etj_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_cdl = "SELECT * FROM cdl_cuadrilla";
                                    $res_cdl = conexionbd()->query($sql_modificar_cdl);                                    
                                ?>
                                    <select class="form-control"name="cdl_id" id="cdl_id">
                                        <option value="" disabled="">Cuadrilla</option>                                                                       
                                        <?php 
                                            if($filas = $res_cdl->num_rows>0){
                                                while($cdl_id = $res_cdl->fetch_assoc()){ 
                                                    if($datos ['cdl_id'] == $cdl_id['cdl_id']){?>
                                                    <option selected="selected" value="<?php echo $cdl_id['cdl_id'];?>"><?php echo $cdl_id['cdl_id'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $cdl_id['cdl_id'];?>"> <?php echo $cdl_id['cdl_id'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                   
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_rol = "SELECT * FROM rol";
                                    $res_rol = conexionbd()->query($sql_modificar_rol);                                    
                                ?>
                                    <select class="form-control"name="rol_id" id="rol_id">
                                        <option value="" disabled="">Rol</option>                                                                       
                                        <?php 
                                            if($filas = $res_rol->num_rows>0){
                                                while($rol_id = $res_rol->fetch_assoc()){ 
                                                    if($datos['rol_id'] == $rol_id['rol_id']){?>
                                                    <option selected="selected" value="<?php echo $rol_id['rol_id'];?>"><?php echo $rol_id['rol_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $rol_id['rol_id'];?>"> <?php echo $rol_id['rol_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="password" id="clave_e" name="clave_e" value="<?php echo $datos['tbj_clave'];?>">
                                </div>
                            </div>
                               
                           </div>     
                           <div class="row">
                                <a href="#" onclick="confirmarC()"id="cancelar_editartrabajador" name="cancelar_editartrabajador" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>

                                <script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="lista-trabajadores.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>
                                <button id="editar_trabajador" name="editar_trabajador" style="margin: auto;" type="submit" class="btn btn-primary col-md-5" >Guardar cambios</button>
                           </div>
                          
                       </form>
                   
        </div>

     
<!-- <script>
    $(document).ready(function () {  
        $('form').submit(function(e){
            var formData ={
                'pyt_nombre': $('#pyt_nombre').val(),
                'tpy_id': $('#tpy_id').val(),
                'pyt_fecha_inicio': $('#pyt_fecha_inicio').val(),
                'pyt_fecha_termino': $('#pyt_fecha_termino').val(),
                'pyt_presupuesto_inicial': $('#pyt_presupuesto_inicial').val(),
                'com_id': $('#com_id').val(),
                'epy_id': $('#epy_id').val()                
            };
            $.ajax({
                type: 'POST',
                url: 'agregar-proyecto.php',
                data: formData,
                dataType: 'json',
                encode: true
            })
            .done(function(data){
                if(!data.success){
                    Swal.fire({
                                    icon: 'error',
                                    title: 'Ha ocurrido un error',
                                    text: 'Debe completar todos los campos'
                                })
                }else{
                    Swal.fire({
                                    icon: 'success',
                                    title: 'Registro exitoso',
                                    text: "La marca se ha registrado con éxito",
                
                                    showCancelButton: false,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Continuar'
                                }).then((result) => {
                                    if (result.value) {
                                        location.href = "../area/lista-area.php";
                                    }
                                })
                }
            });
        });
    });
</script> -->



</body>
</html>