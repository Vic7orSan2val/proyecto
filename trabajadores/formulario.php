<!DOCTYPE html>
<html lang="en">

<head>
    <?php require '../extensiones/head.php' ?>
    <title>Registrar usuario</title>
    <link rel="shortcut icon" type="image/x-icon" href="../img/trabajadores.png">

</head>

<body style="background:linear-gradient(20deg, LightGray, Gainsboro);";>

    <?php include '../extensiones/nav-form-trab.php' ?>


<script type="text/javascript">

window.onload = function () {

document.formulariot.nombre.focus();

document.formulariot.addEventListener('submit', validarFormulario);

}

function validarFormulario(evObject) {

evObject.preventDefault();

var todoCorrecto = true;

var formulario = document.formulariot;

for (var i=0; i<formulario.length; i++) {

                if(formulario[i].type =='text') {

                               if (formulario[i].value == null || formulario[i].value.length == 0 || /^\s*$/.test(formulario[i].value)){

                               alert (formulario[i].name+ ' no puede estar vacío o contener sólo espacios en blanco');

                               todoCorrecto=false;

                               }

                }  
                
                }

if (todoCorrecto ==true) {formulario.submit();}
}
</script>

    <div class="contenedor">

        <h2 class="form_titulo">Formulario <span> Registro</span></h2>

        <div class="form">
            <form name="formulariot" action="agregar-trabajador.php" method="POST">
                <p>
                    <label for="nombre">Nombres</label>
                    <input type="text" placeholder="Ingrese su Nombre" id="nombre" name="nombre" required>
                </p>
                <p>
                    <label for="apellido">Apellidos</label>
                    <input type="text" placeholder="Ingrese su Apellido" id="apellido" name="apellido" required>
                </p>
                <p>
                    <label for="rut">Rut</label>
                    <input type="text" placeholder="12.123.123-3" id="rut" name="rut" maxlength="12" required oninput="checkRut(this)">
                </p>
                <p>
                    <label for="correo">Correo</label>
                    <input type="email" placeholder="Ingrese su Correo" id="correo" name="correo" required>
                </p>
                <p>
                    <label for="telefono">Telefono</label>
                    <input type="tel" placeholder="Ingrese su N°contacto" id="telefono" name="telefono" maxlength="9" required>
                </p>
                <p>
                    <label for="ciudad">Ciudad</label>
                    <input type="text" placeholder="Ingrese su Ciudad" id="ciudad" name="ciudad" required>
                </p>
               <p>
                    <label for="id_doc">N° Documento</label>
                    <input type="number" placeholder="Id Documento" id="id_doc" name="id_doc" required>
                </p>
                <p>
                    <label for="antecedentes">Antecedentes</label>
                    <input type="text" placeholder="Antecedentes" id="antecedentes" name="antecedentes" required>
                </p>
                <p>
                    <label for="afiliacion_afp">Afiliación Afp</label>
                    <input type="text" placeholder="Afp" id="afiliacion_afp" name="afiliacion_afp" required>
                </p>
                <p>
                    <label for="salud">Sistema Salud</label>
                    <input type="text" placeholder="Ingrese su Sistema de Salud" id="salud" name="salud" required>
                </p>
                <p>
                    <label for="id_cuadrilla">Id Cuadrilla</label>
                    <input type="number" placeholder="Id Cuadrilla" id="id_cuadrilla" name="id_cuadrilla" required>
                </p>
                <p>
                    <label for="id_contrato">N° Contrato</label>
                    <input type="number" placeholder="Id Contrato" id="id_contrato" name="id_contrato" required>
                </p>
                <p>
                    <label for="pass">Contraseña</label>
                    <input type="password" placeholder="Pin de 8 digitos" id="pass" name="pass" maxlength="8" required>
                </p>

                <button type="submit" class="button2">Registrar</button>
                <script src="validarRUT.js"></script>

               
            </form>



        </div>
        </form>
    </div>

    <?php require '../extensiones/scripts.php' ?>
</body>

</html>