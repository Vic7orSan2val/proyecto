<?php
    require_once '../config/conexion.php';

    function seleccion_region(){
      

        // $mysqli = conexionbd();
        $sql = "SELECT rgn_id,rgn_nombre FROM rgn_region ORDER BY rgn_id ASC";
        $resultado = conexionbd()->query($sql);

        $seleccion_region = "<option selected='selected' disabled value='0'>Elige una region</option>";
        
        while($row = $resultado->fetch_assoc()){
            $seleccion_region .= "<option value='$row[rgn_id]'>'$row[rgn_nombre]'</option>";
        }
        return $seleccion_region;

    }

    echo seleccion_region();

  