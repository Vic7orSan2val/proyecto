<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Administracion de proyectos</title>
    <?php   require_once '../extensiones/head.php';
            require_once '../config/conexion.php'; 
            require_once '../extensiones/nav_jefeinventario.php'         
    ?>
    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funciones-proyecto.js"></script>
    <!-- <script src="js/validar-pyt.js"></script> -->
</head>
<body>
    <div class="ml-5 mr-5">
        <div class="menu">
            <div class="row">
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">                                                
                        <h4>Administracion de Proyectos</h4><br>                        
                    </div>
                </div>
                <!-- Fin titulo -->             
              
                <br>
                <!--LLAMADA TABLA -->
                    <div class="col-md-12" id="tabla"></div>
                <!-- FIN LLAMADA TABLA --> 

 

                                    <!--MODAL AGREGAR -->
<div class="modal fade" id="m_agregar_proyecto" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Nuevo Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> 
      <div class="modal-body">
      <div class="container ">                   
                       <form action="" method="POST" id="formulario_agregar_pyt" name="formulario_agregar_pyt">
                           <div class="row">
                               <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="pyt_nombre" name="pyt_nombre" placeholder="Ingrese nombre del proyecto"> 
                                    </div>
                               </div>    

                        
                               <div class="col-sm-6">
                                <div class="form-group">

                                <?php                                 
                                    $sql_modificar_tipo = "SELECT * FROM tpy_tipo_proyecto";
                                    $res = conexionbd()->query($sql_modificar_tipo);
                                ?>
                                    <select class="form-control"name="tpy_id" id="tpy_id">
                                        <option value="0" disabled selected>Seleccione Tipo Proyecto</option>
                                        <?php 
                                            while($row = $res->fetch_assoc()){?>
                                                <option value="<?php echo $row['tpy_id']?>"> <?php echo $row['tpy_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div>                       
                             <div class="col-sm-6">
                                 <div class="form-group ">
                                     <label for="pyt_fecha_inicio">Fecha Inicio</label>
                                     <input type="date" max="2300-12-31" min="2020-12-31" class="form-control" id="pyt_fecha_inicio" name="pyt_fecha_inicio">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                <label for="pyt_fecha_termino">Fecha Termino</label>
                                     <input type="date" max="2300-12-31" min="2020-12-31" class="form-control" id="pyt_fecha_termino" name="pyt_fecha_termino">
                                </div>
                            </div>
                    
                               <!--SELECT DINAMICO REGIONES-->
                        <div class="form-group col-md-6">
                            <label for="rgn_id">Seleccione la region</label>
                            <select id="rgn_id" name="rgn_id" class="form-control">                                
                            </select>                                           
                        </div> 
                        <!-- SELECT COMUNAS-->
                        <div class="form-group col-md-6">
                            <label for="com_id">Seleccione la comuna</label>
                                <select id="com_id" name="com_id" class="form-control">   
                                <option value='0'>Elige una comuna</option>             
                                </select>                                           
                        </div>
                        

                            <div class="col-sm-6">
                                <div class="form-group">
                                     <input class="form-control" type="text" id="pyt_presupuesto_inicial" name="pyt_presupuesto_inicial" placeholder="Presupuesto inicial">
                                </div>                            
                            </div>

                            
                            <div class="col-sm-6">
                                <div class="form-group">
                                <?php 
                                    $sql_modificar_estado = "SELECT * FROM epy_estado_proyecto";
                                    $resultado_estado = conexionbd()->query($sql_modificar_estado);
                                ?>
                                    <select class="form-control" name="epy_id" id="epy_id">
                                        <option value="0" disabled selected>--Estado del proyecto--</option>
                                        <?php while($row_estado = $resultado_estado->fetch_assoc()){?>
                                        <option value="<?php echo $row_estado['epy_id']?>"><?php echo $row_estado['epy_nombre'] ?></option>    
                                        <?php }?>
                                    </select>
                                </div>
                            </div>                                                                            
                           </div>     
                           <div class="row">
                                <a href="#" onclick="return confirmarR()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>
                                
                                <button id="agregarproyecto" name="agregarproyecto" style="margin: auto;" type="button" class="btn btn-primary col-md-5" >Registrar</button>
                           </div>
                          
                       </form>
                   
        </div>
      </div>
                <div class="modal-footer" >
      
                </div>
    </div>
  </div>
</div>




           
            </div>
        </div>
    </div>

    
    <?php require_once '../extensiones/scripts.php' ?>
    

</body>
</html>
<script>
//AJAX PARA SELECCIONAR REGIONES Y COMUNAS
$(document).ready(function(){
   // alert('GOLA')
    $.ajax({
        type: 'POST',
        url: 'select-region.php'
    })
    .done(function(resp){
        //alert('TODO BIEN')
        $('#rgn_id').html(resp)
    })
    .fail(function(){
        //alert('HUBI UN ERROOR CONLAS REGIONAS')
    })
    
///AJAX COMUNAS
    $('#rgn_id').on('change', function(){
        var id = $('#rgn_id').val()
        //alert(id)
        $.ajax({
            type: 'POST',
            url: 'select-comuna.php',
            data: {'id': id}
        })
        .done(function(resp){
           // alert('TODO BIEN')
            $('#com_id').html(resp)
        })
        .fail(function(){
            alert('HUBI UN ERROOR CONLAS comuNAS')
        })
    })       
})
  </script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes-p/tablap.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#agregarproyecto').click(function(){  
                //alert('correcto');
                // pyt_nombre=$('#pyt_nombre').val();
                // pyt_fecha_inicio=$('#pyt_fecha_inicio').val();
                // pyt_fecha_termino=$('#pyt_fecha_termino').val();            
                // pyt_presupuesto_inicial=$('#pyt_presupuesto_inicial').val();
                // com_id=$('#com_id').val();
                // epy_id=$('#epy_id').val();
                // tpy_id=$('#tpy_id').val();
                
    //             agregarProyecto(pyt_nombre, pyt_fecha_inicio, pyt_fecha_termino,pyt_presupuesto_inicial,
    // com_id,epy_id,tpy_id)
                agregarProyecto();
           });


        //    $('#editarproyecto').click(function(){
        //         // alert('click');    
        //     modificarProyecto();
        //    });
        });
    </script>

                <script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="lista-proyectos.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>

