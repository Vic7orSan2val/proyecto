function agregarProyecto(){

    var pyt_nombre=  document.getElementById('pyt_nombre').value;
    var tpy_id = document.getElementById('tpy_id').value;
    var pyt_fecha_inicio=  document.getElementById('pyt_fecha_inicio').value;
    var pyt_fecha_termino=  document.getElementById('pyt_fecha_termino').value;          
    var pyt_presupuesto_inicial=  document.getElementById('pyt_presupuesto_inicial').value;
    var com_id=  document.getElementById('com_id').value;            
    var epy_id=  document.getElementById('epy_id').value; 
    var rgn_id = document.getElementById('rgn_id').value;
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO

    if(pyt_nombre===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Campo nombre de proyecto no puede estar vacio'           
          })
          return false; 
    }else if(novale.test(pyt_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre de proyecto no puede tener espacios en blanco al inicio!'            
          })
          return false;
    }
    else if(!expresion.test(pyt_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre de proyecto de proyecto solo permite letras'            
          })
        return false;  
    }     
    else if(tpy_id == 0 || tpy_id == ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe seleccionar un tipo de proyecto'            
          })
        return false; 
    }
    else if(pyt_fecha_inicio === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar una fecha de inicio'            
          })
        return false; 
    }
    else if(pyt_fecha_termino === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar una fecha de termino'            
          })
        return false; 
    }
    else if(pyt_fecha_inicio > pyt_fecha_termino){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'La fecha de inicio no puede ser mayor a la de termino'            
          })
        return false; 
    }
    else if(rgn_id == 0 || rgn_id ==""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una region por favor'            
          })
        return false;
    }
    else if(com_id == 0 || com_id == ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una comuna por favor'            
          })
        return false; 
    }
    else if(pyt_presupuesto_inicial===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar un presupuesto'            
          })
        return false; 
    }
    else if(novale.test(pyt_presupuesto_inicial)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Campo presupuesto no puede contener espacios blancos al inicio'            
          })
        return false; 
    }
    else if(isNaN(pyt_presupuesto_inicial)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Presupuesto deben ser solo numeros'            
          })
        return false;  
    }else if(epy_id == 0 || epy_id == ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un estado por favor'            
          })
        return false;
    }
    else{
// alert('todo bien');
// return false;
cadena =    "pyt_nombre=" + pyt_nombre + 
     "&pyt_fecha_inicio=" + pyt_fecha_inicio + 
     "&pyt_fecha_termino="+pyt_fecha_termino +                
     "&pyt_presupuesto_inicial=" + pyt_presupuesto_inicial +
     "&com_id="+com_id+
     "&epy_id="+epy_id+
     "&tpy_id="+tpy_id;
     


$.ajax({
type: "POST",
url: "phpProyecto/agregarProyecto.php",
data:  cadena,
success: function(r){
 if(r==1){
     $('#tabla').load('componentes-p/tablap.php');
     //alert("Agregado con exito");
     Swal.fire({
         icon: 'success',
         title: 'Registro exitoso',
         text: "La marca se ha registrado con éxito",

         showCancelButton: false,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Continuar'
     }).then((result) => {
         if (result.value) {
             location.href = "../proyecto/lista-proyectos.php";
         }
     }) 
 }
 else{
    // alert("ERROR, NO AGREGADO");
     Swal.fire({
         icon: 'error',
         title: 'Ha ocurrido un error',
         text: 'Debe completar todos los campos'
     })
 }
}
});
}
}

// function datosform(datos){
//     d = datos.split('||');
//     $('#pyt_id_edit').val(d[0]),
//     $('#pyt_nombre_edit').val(d[1]), 
//     $('#tpy_id_edit').val(d[7]),
//     $('#pyt_fecha_inicio_edit').val(d[2])  ,
//     $('#pyt_fecha_termino_edit').val(d[3])  ,    
//     $('#pyt_presupuesto_inicial_edit').val(d[4])  ,
//     $('#com_id_edit').val(d[5])  ,
//     $('#epy_id_edit').val(d[6])
// }

// function modificarProyecto(){
//     pyt_id = $('#pyt_id_edit').val(),
//     pyt_nombre=  $('#pyt_nombre_edit').val(),
//     tpy_id = $('#tpy_id_edit').val(),
//     pyt_fecha_inicio = $('#pyt_fecha_inicio_edit').val()  ,
//     pyt_fecha_termino= $('#pyt_fecha_termino_edit').val()  ,
//     pyt_presupuesto_inicial=    $('#pyt_presupuesto_inicial_edit').val()  ,
//     com_id= $('#com_id_edit').val()  ,
//     epy_id =  $('#epy_id_edit').val()

//     cadena =    "pyt_id="+pyt_id+
//                 "&pyt_nombre=" + pyt_nombre + 
//                 "&pyt_fecha_inicio=" + pyt_fecha_inicio + 
//                 "&pyt_fecha_termino="+pyt_fecha_termino +
     
//                 "&pyt_presupuesto_inicial=" + pyt_presupuesto_inicial +
//                 "&com_id="+com_id+
//                 "&epy_id="+epy_id+
//                 "&tpy_id="+tpy_id;
     
//                 $.ajax({
//                     type: "POST",
//                     url: "phpProyecto/modificarProyecto.php",
//                     data:  cadena,
//                     success: function(r){
//                         if(r==1){
//                             $('#tabla').load('componentes-p/tablap.php');
//                             //alert("Agregado con exito");
//                             Swal.fire({
//                                 icon: 'success',
//                                 title: 'Registro exitoso',
//                                 text: "La marca se ha registrado con éxito",
 
//                                 showCancelButton: false,
//                                 confirmButtonColor: '#3085d6',
//                                 cancelButtonColor: '#d33',
//                                 confirmButtonText: 'Continuar'
//                             }).then((result) => {
//                                 if (result.value) {
//                                     location.href = "../proyecto/lista-proyectos.php";
//                                 }
//                             })
//                         }
//                         else{
//                            // alert("ERROR, NO AGREGADO");
//                             Swal.fire({
//                                 icon: 'error',
//                                 title: 'Ha ocurrido un error',
//                                 text: 'Debe completar todos los campos'
//                             })
//                         }
//                     }
//                 });
     
// }// // ELIMINAR
function confirmarEliminarProyecto(pyt_id){

// alertify.confirm('Eliminar Proyecto','¿Seguro que desea eliminar?', 
//     function()
//     {   eliminarProyecto(pyt_id)},
//     // alertify.success('Si') } ,
//     function(){
//     alertify.message('Eliminación cancelada')}); 
//     // alert('hola')
const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
    },
    buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
    title: '¿Está seguro que desea eliminar el proyecto?',
    text: "No podrá recuperar el registro guardado!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: '¡Sí, eliminar!',
    cancelButtonText: '¡No, cancelar!',
    reverseButtons: false,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    }).then((result) => {
    if (result.value) {
        eliminarProyecto(pyt_id)
    } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
    ) {
        swalWithBootstrapButtons.fire(
        'Cancelado',
        'El proyecto no se eliminará',
        'error'
        )
    }
    })
}

function eliminarProyecto(pyt_id){
    cadena="pyt_id=" + pyt_id;

    $.ajax({
        type: "POST",
        url: 'phpProyecto/eliminarProyecto.php' ,
        data: cadena ,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes-p/tablap.php');
                // alertify.success("Eliminado con exito");
                Swal.fire({
                    icon: 'success',
                    title: 'Eliminación exitosa',
                    text: "Proyecto eliminado",

                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
            
            }else{
                // alertify.error("Error");
                Swal.fire({
                    icon: 'error',
                    title: 'No se ha podido eliminar',
                    text: 'Ocurrió un error interno'
                })
            }
            }
    });
}