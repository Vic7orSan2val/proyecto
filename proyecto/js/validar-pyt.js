function agregarProyecto(){
    
    var pyt_nombre=  document.getElementById('pyt_nombre').value;
    var tpy_id = document.getElementById('tpy_id').value;
    var pyt_fecha_inicio=  document.getElementById('pyt_fecha_inicio').value;
    var pyt_fecha_termino=  document.getElementById('pyt_fecha_termino').value;          
    var pyt_presupuesto_inicial=  document.getElementById('pyt_presupuesto_inicial').value;
    var com_id=  document.getElementById('com_id').value;            
    var epy_id=  document.getElementById('epy_id').value;                           
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO

    if(pyt_nombre===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Campo nombre de proyecto no puede estar vacio'           
          })
          return false; 
    }else if(novale.test(pyt_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre de proyecto no puede tener espacios en blanco al inicio!'            
          })
          return false;
    }
    else if(!expresion.test(pyt_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre de proyecto de proyecto solo permite letras'            
          })
        return false;  
    }     
    else if(tpy_id == 0 || tpy_id == ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe seleccionar un tipo de proyecto'            
          })
        return false; 
    }
    else if(pyt_fecha_inicio === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar una fecha de inicio'            
          })
        return false; 
    }
    else if(pyt_fecha_termino === ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar una fecha de termino'            
          })
        return false; 
    }
    else if(pyt_fecha_inicio > pyt_fecha_termino){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'La fecha de inicio no puede ser mayor a la de termino'            
          })
        return false; 
    }
    // else if(rgn_id == 0 || rgn_id ==""){
    //     Swal.fire({
    //         icon: 'error',
    //         title: 'Error...',
    //         text: 'Seleccione una region por favor'            
    //       })
    //     return false;
    // }
    else if(com_id == 0 || com_id == ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione una comuna por favor'            
          })
        return false; 
    }
    else if(pyt_presupuesto_inicial===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar un presupuesto'            
          })
        return false; 
    }
    else if(novale.test(pyt_presupuesto_inicial)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Campo presupuesto no puede contener espacios blancos al inicio'            
          })
        return false; 
    }
    else if(isNaN(pyt_presupuesto_inicial)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Presupuesto deben ser solo numeros'            
          })
        return false;  
    }else if(epy_id == 0 || epy_id == ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un estado por favor'            
          })
        return false;
    }else{
      Swal.fire({
        icon: 'success',
        title: 'Modificacion exitosa...',
        text: 'La modificacion se ha realizado con exito'           
      })
    }
    }