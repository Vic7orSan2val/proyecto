<?php require_once '../sesiones/sesion.php';?>

<?php
    require_once '../config/conexion.php'; 
    
    $modificar = $_GET['id'];

    $sql = "SELECT  p.pyt_id, p.pyt_nombre, p.pyt_fecha_inicio, p.pyt_fecha_termino,
                    p.pyt_presupuesto_inicial, c.com_nombre, epy.epy_nombre, tpy.tpy_nombre, tpy.tpy_id,c.com_id,
                    epy.epy_id
            FROM pyt_proyecto p INNER JOIN com_comuna c ON p.com_id = c.com_id INNER JOIN
            tpy_tipo_proyecto tpy ON p.tpy_id = tpy.tpy_id INNER JOIN
            epy_estado_proyecto epy ON p.epy_id = epy.epy_id
            WHERE p.pyt_id = $modificar";   

    $resultado = conexionbd()->query($sql);

    $datos = $resultado->fetch_array();

?>

<!DOCTYPE html>
<html lang="en">
<head>    
    <title>Editar proyecto</title>
    
    <?php   require_once '../extensiones/head.php';            
            require_once '../extensiones/nav_jefeinventario.php' ;
            require_once '../extensiones/scripts.php' ;

    ?>    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <!-- <script src="js/funciones-proyecto.js"></script> --> 
    <!-- <script src="js/validar-proyecto.js"></script> -->
    <script src="js/validar-pyt.js"></script>
</head>
<body>
<div class="container mt-5 mb-5">                   
<h3 class="mb-4">Editar Proyecto</h3>
    <form action="phpProyecto/editar-proyecto.php" method="POST" id="formulario_editar" name="formulario_editar" onsubmit="return agregarProyecto();">
        
                        <div class="row">
                            <input type="hidden" name="id_p" id="id_p" value="<?php echo $datos['pyt_id']; ?>">

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="pyt_nombre" name="pyt_nombre" value="<?php echo $datos['pyt_nombre'];?>"> 
                                </div>
                            </div>    
                                                                                
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_tpy = "SELECT tpy_id, tpy_nombre FROM tpy_tipo_proyecto";
                                    $res = conexionbd()->query($sql_modificar_tpy);                                    
                                ?>
                                    <select class="form-control"name="tpy_id" id="tpy_id">
                                        <option value="" disabled="">Tipo de proyecto</option>                                                                       
                                        <?php 
                                            if($filas = $res->num_rows>0){
                                                while($tpy_id = $res->fetch_assoc()){ 
                                                    if($datos['tpy_id'] == $tpy_id['tpy_id']){?>
                                                    <option selected="selected" value="<?php echo $tpy_id['tpy_id'];?>"><?php echo $tpy_id['tpy_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $tpy_id['tpy_id'];?>"> <?php echo $tpy_id['tpy_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div> 
                            </div>  

                             <div class="col-sm-6">
                                 <div class="form-group ">
                                     <label for="pyt_fecha_inicio">Fecha Inicio</label>
                                     <input type="date" class="form-control" id="pyt_fecha_inicio" name="pyt_fecha_inicio"  value="<?php echo $datos['pyt_fecha_inicio'];?>" >
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                <label for="pyt_fecha_termino">Fecha Termino</label>
                                     <input type="date" max="2300-12-31" min="2020-12-31" class="form-control" id="pyt_fecha_termino" name="pyt_fecha_termino" value="<?php echo $datos['pyt_fecha_termino'];?>">
                                </div>
                            </div>
                    
                             
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="pyt_presupuesto_inicial" name="pyt_presupuesto_inicial"  value="<?php echo $datos['pyt_presupuesto_inicial'];?>">
                                </div>                            
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_com = "SELECT com_id, com_nombre FROM com_comuna";
                                    $res_com = conexionbd()->query($sql_modificar_com);                                    
                                ?>
                                    <select class="form-control"name="com_id" id="com_id">
                                        <option value="" disabled="">Tipo de proyecto</option>                                                                       
                                        <?php 
                                            if($filas_com = $res_com->num_rows>0){
                                                while($com_id = $res_com->fetch_assoc()){ 
                                                    if($datos['com_id'] === $com_id['com_id']){?>
                                                    <option selected="selected" value="<?php echo $com_id['com_id'];?>"><?php echo $com_id['com_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $com_id['com_id'];?>"> <?php echo $com_id['com_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>  
                           
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_epy = "SELECT * FROM epy_estado_proyecto";
                                    $res_epy = conexionbd()->query($sql_modificar_epy);                                    
                                ?>
                                    <select class="form-control"name="epy_id" id="epy_id">
                                        <option value="" disabled="">Tipo de proyecto</option>                                                                       
                                        <?php 
                                            if($filas_epy = $res_epy->num_rows>0){
                                                while($epy_id = $res_epy->fetch_assoc()){ 
                                                    if($datos ['epy_id'] == $epy_id['epy_id']){?>
                                                    <option selected="selected" value="<?php echo $epy_id['epy_id'];?>"><?php echo $epy_id['epy_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $epy_id['epy_id'];?>"> <?php echo $epy_id['epy_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div> 

                                                                                                       
                           </div>     
                           <div class="row">
                                <a href="#" onclick="confirmarC()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>

                                <button id="editarproyecto" name="editarproyecto" style="margin: auto;" type="submit" class="btn btn-primary col-md-5">Guardar cambios</button>
                           </div>
                          
                       </form>
                   
        </div>

     </body>
</html>
<script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="lista-proyectos.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>