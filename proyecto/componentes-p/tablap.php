<?php 
    include '../../config/conexion.php';
    // $sql = "SELECT  pyt_id , com_id , epy_id, tpy_id, pyt_nombre , pyt_fecha_inicio ,pyt_fecha_termino,
    //                 pyt_ubicacion,pyt_presupuesto_inicial
    // FROM pyt_proyecto pyt 
    // LEFT JOIN com_comuna com ON  pyt.com_id = com.com_id
    // lEFT JOIN tpy_tipo_proyecto tpy ON pyt.tpy_id = tpy.tpy_id
    // LEFT JOIN epy_estado_proyecto epy ON pyt.epy_id = epy.epy_id";
    $sql = "SELECT  p.pyt_id, p.pyt_nombre, p.pyt_fecha_inicio, p.pyt_fecha_termino,
                    p.pyt_presupuesto_inicial, c.com_nombre, epy.epy_nombre, tpy.tpy_nombre
                    ,rgn.rgn_nombre
                FROM pyt_proyecto p JOIN com_comuna c ON p.com_id = c.com_id JOIN
                tpy_tipo_proyecto tpy ON p.tpy_id = tpy.tpy_id JOIN
                epy_estado_proyecto epy ON p.epy_id = epy.epy_id JOIN
                rgn_region rgn ON c.rgn_id = rgn.rgn_id";  

    $resultado = conexionbd()->query($sql);

    // Creación del array con los datos 
    $resultado2 = mysqli_query($conexion,$sql) or die (mysql_error ());

    $proyectos = array();
    
    while( $rows = mysqli_fetch_assoc($resultado2) ) {
    
    $proyectos[] = $rows;
    
    }

    // Función que crea el excel

    if(isset($_POST["export_data"])) {

        if(empty($proyectos)) {?>
            <script>
            alert('NO HAY DATOS PARA EXPORTAR')
            window.location = "../lista-proyectos.php"
            </script>
            
            <?php
        }else{
            
            $filename = "Proyectos.xls";
            
            header("Content-Type: application/vnd.ms-excel");
            
            header("Content-Disposition:attachment; filename=".$filename);
            
            $mostrar_columnas = false;
    
            foreach($proyectos as $proyecto) {
    
                if(!$mostrar_columnas) {
    
                    echo implode("\t", array_keys($proyecto)) . "\n";
                    $mostrar_columnas = true;
                }
            echo implode("\t", array_values($proyecto)) . "\n";
            }
        }
    exit;
    }
    
?>

 <!-- Inicio tabla -->
<div class="row">
    <div class="table-responsive">
        <caption>
        <!-- <a href="#" class="btn btn-block btn-success">Agregar</a> -->
        <button class="col-sm-4 btn btn-block btn-success"data-toggle="modal" data-target="#m_agregar_proyecto">Agregar </button>
        </caption><br>
        <table class="table table-bordered" id="datatable">
            <thead class="thead-dark">
                <tr>
                    <th>Nombre Proyecto</th>
                    <th>Fecha Inicio</th>
                    <th>Fecha Termino</th>                    
                    <th>Presupuesto Inicial</th>
                    <!-- REVISAR -->
                    <th>Region</th>
                    <th>Comuna</th>
                    <th>Estado Proyecto</th>
                    <th>Tipo de proyecto</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($resultado->num_rows>0):
                    while($proyecto = $resultado->fetch_assoc()):

                        //$datos = $proyecto['tpy_id']."||".$proyecto['tpy_nombre']."||".$proyecto['tpy_descripcion'];
                       $datos =     $proyecto['pyt_id']."||".
                                    $proyecto['pyt_nombre']."||".
                                    $proyecto['pyt_fecha_inicio']."||".
                                    $proyecto['pyt_fecha_termino']."||".                                    
                                    $proyecto['pyt_presupuesto_inicial']."||".
                                    $proyecto['com_nombre']."||".
                                    $proyecto['epy_nombre']."||".
                                    $proyecto['tpy_nombre'];
                ?>
                <tr>
                    <td><?php echo $proyecto['pyt_nombre'] ?></td>
                    <td><?php echo $proyecto['pyt_fecha_inicio']?></td>
                    <td><?php echo $proyecto['pyt_fecha_termino']?></td>                    
                    <td><?php echo $proyecto['pyt_presupuesto_inicial']?></td>
                    <td><?php echo $proyecto['rgn_nombre']?></td>
                    <td><?php echo $proyecto['com_nombre']?></td>
                    <td><?php echo $proyecto['epy_nombre']?></td>
                    <td><?php echo $proyecto['tpy_nombre']?></td>
                   
                    <td>
                        <div class="row">
                            <!-- <div class="col-sm-6">                                
                                <a href="#" class="btn btn-block btn-info" data-toggle="modal"
                                 data-target="#m_editar_proyecto" onclick="datosform('<?php // echo $datos?>')">Modificar</a>
                            </div> -->
                            <div class="col-md-6">
                            <a class="btn btn-block btn-info" href="form-editar-proyecto.php?id=<?php echo $proyecto['pyt_id']?>">Modificar</a>
                            </div>
                            <div class="col-sm-6">
                                <!-- <button type="submit" onclick="confirmar(<?php //echo $proyecto['tpy_id'] ?>)" class="btn btn-block btn-danger">Eliminar</button> -->
                                <button type="submit" class="btn btn-block btn-danger" onclick="confirmarEliminarProyecto(<?php echo $proyecto['pyt_id']?>)">Eliminar</button>
                        </div>
                    </td>
                </tr>
                <?php 
                    endwhile;
                endif;                
                ?>
            </tbody>
        </table>
    </div>
</div>
                <!-- Fin tabla -->

<div class="btn-group pull-right">
    <form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data'
            value="Export to excel" class="btn btn-info">Exportar a Excel</button>
    </form>
</div>  
                
  