<?php 
    require_once '../../config/conexion.php';
    $select = "SELECT * FROM  tpy_tipo_proyecto";
    $resultado = conexionbd()->query($select);
?>

<div >
    <div class="col-sm-12">

    </div class="row">
    <div class="col-sm-8">
        <label>Buscador</label>
        <select class="form-control" name="buscadortipo" id="buscadortipo">
            <option value="0">Selecciona</option>
            <?php 
                while($datos = $resultado->fetch_assoc()):
            ?>
                <option value="<?php echo $datos['tpy_id'] ?> ">
                    <?php echo $datos['tpy_nombre']?>
                </option>


            <?php endwhile;?>
        </select>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () { 
        $('#buscadortipo').select2();

        $('#buscadortipo').change(function () {  
            $.ajax({
                type: "POST",
                // data: , 
                url: "phpTipo/crearsesion.php",
                success: function(r){
                    $('#buscador').load('componentes/buscador.php');
                }
            });
        });
     });
</script>