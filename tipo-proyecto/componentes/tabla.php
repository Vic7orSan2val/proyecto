<?php 
    include '../../config/conexion.php';
    $sql = "SELECT * FROM tpy_tipo_proyecto";
    $resultado = conexionbd()->query($sql);

    // Creación del array con los datos 
    $resultado2 = mysqli_query($conexion,$sql) or die (mysql_error ());

    $tipos = array();
    
    while( $rows = mysqli_fetch_assoc($resultado2) ) {
    
    $tipos[] = $rows;
    
    }

    // Función que crea el excel

    if(isset($_POST["export_data"])) {

        if(empty($tipos)) { ?>
            <script>
                    alert('NO EXISTEN DATOS PARA EXPORTAR')
                    window.location = "../listar-tipos-proyecto.php"
                </script>
            <?php
        }else{
            
            $filename = "Tipo-Proyectos.xls";
            
            header("Content-Type: application/vnd.ms-excel");
            
            header("Content-Disposition:attachment; filename=".$filename);
            
            $mostrar_columnas = false;
    
            foreach($tipos as $tipo) {
    
                if(!$mostrar_columnas) {
    
                    echo implode("\t", array_keys($tipo)) . "\n";
                    $mostrar_columnas = true;
                }
            echo implode("\t", array_values($tipo)) . "\n";
            }
        }
    exit;
    }
    
?>

 <!-- Inicio tabla -->
<div class="row">
    <div class="table-responsive">
        <caption>
        <!-- <a href="#" class="btn btn-block btn-success">Agregar</a> -->
        <button class="col-sm-4 btn btn-block btn-success"data-toggle="modal" data-target="#m_agregar_tipo">Agregar </button>
        </caption><br>
        <table class="table table-bordered" id="datatable">
            <thead class="thead-dark">
                <tr>
                    <th>Nombre Tipo Proyecto</th>
                    <th>Descripcion</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($resultado->num_rows>0):
                    while($tipos = $resultado->fetch_assoc()):

                        $datos = $tipos['tpy_id']."||".$tipos['tpy_nombre']."||".$tipos['tpy_descripcion'];
                       
                ?>
                <tr>
                    <td><?php echo $tipos['tpy_nombre'] ?></td>
                    <td><?php echo $tipos['tpy_descripcion']?></td>
                   
                    <td>
                        <div class="row">
                            <div class="col-md-6">                                
                                <a href="#" class="btn btn-block btn-info" data-toggle="modal"
                                 data-target="#m_editar_tipo" onclick="agregaForm('<?php echo $datos?>')">Modificar</a>
                            </div>

                            <div class="col-md-6">
                                <!-- <button type="submit" onclick="confirmar(<?php //echo $tipos['tpy_id'] ?>)" class="btn btn-block btn-danger">Eliminar</button> -->
                                <button type="submit" class="btn btn-block btn-danger" onclick="confirmarEliminar(<?php echo $tipos['tpy_id']?>)">Eliminar</button>
                        </div>
                    </td>
                </tr>
                <?php 
                    endwhile;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</div>



                <!-- Fin tabla -->

<div class="btn-group pull-right">
    <form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data'
            value="Export to excel" class="btn btn-info">Exportar a Excel</button>
    </form>
</div>  
                
  