<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Tipos de proyecto</title>
    <?php require_once '../extensiones/head.php' ?>
    <?php require_once '../extensiones/nav_jefeinventario.php' ?>
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funciones-tipo.js"></script>
    <!-- <script src="js/validar-tipo.js"></script> -->
</head>
<body>
    <div class="container">
        <div class="menu">
            <div class="row">
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">                                                
                        <h4>Tipos de proyecto</h4><br>                        
                    </div>
                </div>
                <!-- Fin titulo -->

                <!-- BUSCADOR -->

                
                <!-- <div id="buscador" class="col-md-12"></div><br>
                <br> -->
                <br>
                <!--LLAMADA TABLA -->
                    <div class="col-md-12" id="tabla"></div>
                <!-- FIN LLAMADA TABLA -->



                                    <!--MODAL AGREGAR -->
<div class="modal fade" id="m_agregar_tipo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Tipo de Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"> 
      <div class="container col-md-12">
                   <div class="formulario">
                       <form action="" method="POST" id="formulario_agregar" name="formulario_agregar">
                           <div class="row">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tpy_nombre" name="tpy_nombre" placeholder="Ingrese nombre del tipo"> 
                                    </div>
                               </div>                           
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tpy_descripcion" name="tpy_descripcion" placeholder="Ingrese una descripcion">
                                    </div>
                               </div>
                           </div>                          
                       </form>
                   </div>
        </div>
      </div>
      <div class="modal-footer" >
        <a href="#" onclick="confirmarR()" id="cancelartipo" name="cancelartipo" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cerrar</a>
        <script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="listar-tipo-proyecto.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>
        <button id="agregartipo" name="agregartipo" style="margin: auto;" type="submit" class="btn btn-primary col-md-5">Registrar</button>
        
      </div>
    </div>
  </div>
</div>

 
<!-- EDITAR -->

                                                <!-- INICIO MODAL EDITAR -->
<div class="modal fade" id="m_editar_tipo" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Editar Tipo de Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div name="formulario_editar" id="formulario_editar">
                         <form action="" method="POST">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" hidden="" id="tpy_id_editar" name="tpy_id_editar">
                                </div> 
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="tpy_nombre_editar" name="tpy_nombre_editar" > 
                                        </div>
                                </div>                           
                                <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="tpy_descripcion_editar" name="tpy_descripcion_editar">
                                        </div>
                                </div>                                
                           </div>                         

                       </form>
                   </div>
      </div>
      <div class="modal-footer">
        <a href="#" id="cancelareditartipo" name="cancelareditartipo" style="margin: auto;" 
            type="button" class="btn btn-danger col-md-5" onclick="return confirmarC();">Cancelar
            <!-- onclick="return confirmDelete() -->
            <script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="listar-tipo-proyecto.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>
        </a>
        
        <button id="editartipo" name="editartipo" style="margin: auto;" 
            type="submit" class="btn btn-primary col-md-5">Guardar cambios
        </button>
      </div>
    </div>
  </div>
</div>
                                        <!-- FIN MODAL EDITAR -->

           
            </div>
        </div>
    </div>

    
    <?php require_once '../extensiones/scripts.php' ?>
    

</body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes/tabla.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#agregartipo').click(function(){  
                //alert('correcto');
                // tpy_nombre=$('#tpy_nombre').val();
                // tpy_descripcion=$('#tpy_descripcion').val();
                // agregardatos(tpy_nombre, tpy_descripcion)
                agregardatos()
           });


           $('#editartipo').click(function(){
                // alert('click');     
            modificarTipo();
           });




           


        });
    </script>



