//FUNCION PARA AGREGAR 
function agregardatos(){

    var tpy_nombre = $('#tpy_nombre').val();
    var tpy_descripcion = $('#tpy_descripcion').val();
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var novale = /^\s/; //NO ESPACIO AL PRINCIPIO

    
    if(tpy_nombre==="" && tpy_descripcion===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false;        
    }
    else if(tpy_nombre ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre no puede estar vacio!'           
          })
          return false;
    }
    else if(novale.test(tpy_nombre)){        
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre no puede tener espacios en blanco al inicio!'            
          })
          return false;
    }else if(!expresion.test(tpy_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo tipo de proyecto solo permite letras'            
          })
        return false;
    }
    else if(tpy_descripcion ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo descripcion no puede estar vacio!'          
          })
          return false;
    }
    else if(novale.test(tpy_descripcion)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo descripcion no permite espacios en blanco al inicio!'          
          })
          return false;
    }
    
    else if(!expresion.test(tpy_descripcion)){
         Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo descripcion solo puede contener letras!'          
          })
          return false;
    }
    else{
        // alert('Todo bien');
        // return false;
    cadena = "tpy_nombre=" + tpy_nombre + "&tpy_descripcion=" + tpy_descripcion ;

    $.ajax({
        type: "POST",
        url: "phpTipo/agregartipo.php",
        data:  cadena,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes/tabla.php');
                //alert("Agregado con exito");
            //    alertify.success("Agregado con exito")
            Swal.fire({
                icon: 'success',
                title: '¡Registro exitoso!',
                text: 'El tipo de proyecto se registro correctamente'
            })
            }
            else{
               // alert("ERROR, NO AGREGADO");
            //    alertify.error("Datos no guardados");
            Swal.fire({
                icon: 'error',
                title: '¡Registro fallido!',
                text: 'Ha ocurrido un fallo en el servidor'
            })
            }
        }
    });

}
}
// FUNCIONES EDITAR
function agregaForm(datos){ //OBTENER LOS DATOS DE LA TUPLA DE LA TABLA Y DEJARLAS EN EL FORM PA EDITAR
    //alert(datos);
    d= datos.split('||');
    $('#tpy_id_editar').val(d[0]),
    $('#tpy_nombre_editar').val(d[1]),
    $('#tpy_descripcion_editar').val(d[2])    
}

function modificarTipo(){
    var tpy_id = $('#tpy_id_editar').val();
    var tpy_nombre = $('#tpy_nombre_editar').val();
    var tpy_descripcion = $('#tpy_descripcion_editar').val();
    var expresion = /[a-zA-ZÀ-ÿ]/;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO

    if(tpy_nombre==="" && tpy_descripcion===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false;        
    }
    else if(tpy_nombre ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre no puede estar vacio!'           
          })
          return false;
    }
    else if(novale.test(tpy_nombre)){        
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo nombre no puede tener espacios en blanco al inicio!'            
          })
          return false;
    }else if(!expresion.test(tpy_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo tipo de proyecto solo permite letras'            
          })
        return false;
    }
    else if(tpy_descripcion ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo descripcion no puede estar vacio!'          
          })
          return false;
    }
    else if(novale.test(tpy_descripcion)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo descripcion no permite espacios en blanco al inicio!'          
          })
          return false;
    }
    
    else if(!expresion.test(tpy_descripcion)){
         Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El campo descripcion solo puede contener letras!'          
          })
          return false;
    }
    else{

    
    cadena = "tpy_id=" + tpy_id + "&tpy_nombre=" + tpy_nombre + "&tpy_descripcion=" + tpy_descripcion ;

    $.ajax({
        type: "POST",
        url: "phpTipo/modificartipo.php",
        data:  cadena,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes/tabla.php');
                // alert("Modificado con exito");
                Swal.fire({
                    icon: 'success',
                    title: 'Modificación exitosa',
                    text: "Se han guardado exitosamente los cambios",

                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.href = "../tipo-proyecto/listar-tipo-proyecto.php";
                    }
                })
            }
            else{
            //    alert("ERROR, NO MODIFICADO"); 
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error',
                    text: 'Tipo de proyecto no agregado, verifique todos los campos'
                })
            }
        }
    });
}
    
}
// FIN FUNCIONES EDITAR

// // ELIMINAR
function confirmarEliminar(tpy_id){
   
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
        })

        swalWithBootstrapButtons.fire({
        title: '¿Está seguro que desea eliminar el registro?',
        text: "No podrá recuperar el registro guardado!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: '¡Sí, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        reverseButtons: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        }).then((result) => {
        if (result.value) {
            eliminarTipo(tpy_id)
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelado',
            'El tipo de proyecto no se eliminará',
            'error'
            )
        }
        })



}

function eliminarTipo(tpy_id){
    cadena="tpy_id=" + tpy_id;

    $.ajax({
        type: "POST",
        url: 'phpTipo/eliminartipo.php' ,
        data: cadena ,
        success: function(r){
            if(r==1){
                $('#tabla').load('componentes/tabla.php');
                // alertify.success("Eliminado con exito");
                Swal.fire({
                    icon: 'success',
                    title: 'Eliminación exitosa',
                    text: "Tipo de proyecto eliminado",

                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Continuar'
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })

            }else{
                // alertify.error("Error");
                Swal.fire({
                    icon: 'error',
                    title: 'No se ha podido eliminar',
                    text: 'Ocurrió un error interno'
                })

            }
        }
    });
}