<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once '../extensiones/head.php'; ?>    
    <title>Bienvenido a Constructora ARA</title>
    <link rel="shortcut icon" type="image/x-icon" href="../imagenes/casco.ico">
    <link rel="stylesheet" href="../css/estilos.css">

</head>
<body>
    <?php require_once '../extensiones/nav.php'; ?>    
<div class="titulo">
    <h3 >Bienvenidos a Constructora ARA <br> </h3>
</div>
<div class="slider">
    <ul>
        <li><img src="../imagenes/2.jpg" alt=""></li>
        <li><img src="../imagenes/1.jpg" alt=""></li>
        <li><img src="../imagenes/3.jpg" alt=""></li>
        <li><img src="../imagenes/4.jpg" alt=""></li>
    </ul>

</div>


    <div class="container mt-4 bg-light">
        

       <div class="texto">
       <span>
        Bienvenido a la constructora más reconocida del país. Somos especialistas en lo que hacemos y lo que hacemos siempre 
        será garantia de calidad, seguridad y confianza. A lo largo del país tenemos cientos de proyectos activos y terminados, en
        los cuales siempre destacan nuestros avances tecnologicos en cuanto a construcción y diseño.
        </span> 
       </div>

    </div>


    <!-- <script src="librerias/jquery-3.5.1.min.js"></script> -->
    <?php require_once '../extensiones/scripts.php';?>
</body>
</html>

