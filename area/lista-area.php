<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Areas de trabajo</title>
    <?php 
    require_once '../config/conexion.php';
    require_once '../extensiones/head.php' ;
    require_once '../extensiones/nav_jefecuadrilla.php' ;?>
    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funciones-a.js"></script>
    <script src="js/validar-tipo.js"></script>
</head>
<body>
    <div class="container">
        <div class="menu">
            <div class="row">
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">                                                
                        <h4>Areas de trabajo</h4><br>                        
                    </div>
                </div>
                <!-- Fin titulo -->

                <!-- BUSCADOR -->

                
                <!-- <div id="buscador" class="col-md-12"></div><br>
                <br> -->
                <br>
                <!--LLAMADA TABLA -->
                    <div class="col-md-12" id="tabla"></div>
                <!-- FIN LLAMADA TABLA -->



                                    <!--MODAL AGREGAR -->
<div class="modal fade" id="m_add_area" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Area de trabajo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container col-md-12">
                   <div class="formulario">
                       <form action="" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validar()">
                           <div class="row">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="are_nombre" name="are_nombre" placeholder="Ingrese nombre del area de trabajo"> 
                                    </div>
                               </div>                           
                               <div class="col-sm-12">
                                <div class="form-group">

                                <?php 
                                
                                    $sql_add = "SELECT pyt_id,pyt_nombre FROM pyt_proyecto";
                                    $res = conexionbd()->query($sql_add);
                                ?>
                                    <select class="form-control"name="pyt_id_a" id="pyt_id_a">
                                        <option value="0" disabled selected>Seleccione Proyecto</option>
                                        <?php 
                                            while($row = $res->fetch_assoc()){?>
                                                <option value="<?php echo $row['pyt_id']?>"> <?php echo $row['pyt_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div> 
                           </div>                          
                       </form>
                   </div>
        </div>
      </div>
      <div class="modal-footer" >
        <a href="#" onclick="return confirmarR()" id="cancelartipo" name="cancelartipo" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>
        <script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="lista-area.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>

        <button id="addarea" name="addarea" style="margin: auto;" type="submit" class="btn btn-primary col-md-5">Registrar</button>
      </div>
    </div>
  </div>
</div>

 
<!-- EDITAR -->

                                                <!-- INICIO MODAL EDITAR -->
<div class="modal fade" id="m_editar_area" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Editar Tipo de Proyecto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div name="formulario_editar" id="formulario_editar">
      <form action="" method="POST" id="formulario_editar_area" name="formulario_agregar" onsubmit="return validar()">
                           <div class="row">

                           <div class="col-md-12">
                               <input type="text" hidden="" id="are_id" name="are_id">
                           </div>

                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="are_nombre_e" name="are_nombre_e" placeholder="Ingrese nombre del area de trabajo"> 
                                    </div>
                               </div>                           
                               
                            <!--  -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php 
                                    
                                    $distinta = "SELECT DISTINCT pyt_nombre FROM pyt_proyecto";
                                    $sql = conexionbd()->query($distinta);

                                    $sql_add = "SELECT pyt_id, pyt_nombre FROM pyt_proyecto";
                                    $res = conexionbd()->query($sql_add);
                                    $row = $res->fetch_array();
                                ?>
                                    <select class="form-control"name="pyt_id_e" id="pyt_id_e">
                                        <option value="" disabled>Seleccione Proyecto</option>                                      
                                        <?php foreach($sql as $rows): ?>
                                            <option value="<?php echo $rows['pyt_nombre'];?>" <?php if($row['pyt_nombre'] == $rows['pyt_nombre']) echo 'selected="selected"'?>><?php echo $rows['pyt_nombre']?></option>                                                                     
                                            <?php endforeach;?>
                                        </select>
                                </div> 
                            </div>

                            <!--  -->
                           </div>                          
                       </form>
                       
                   </div>
      </div>
      <div class="modal-footer">
        <button id="cancelareditararea" name="cancelareditararea" style="margin: auto;" 
            type="button" class="btn btn-danger col-md-5" data-dismiss="modal" >Cancelar
            <!-- onclick="return confirmDelete() -->
            <!-- <script>
                    function confirmDelete()
                    {
                        var respuesta = confirm("¿Estás seguro que deseas cancelar la operación?")
                        if (respuesta == true){
                            return true
                        }
                        else{
                            return false;
                        }
                    }
                </script> -->
        </button>
        
        <button id="editararea" name="editararea" style="margin: auto;" 
            type="button" class="btn btn-primary col-md-5">Guardar cambios
        </button>
      </div>
    </div>
  </div>
</div>
                                        <!-- FIN MODAL EDITAR -->

           
            </div>
        </div>
    </div>

    
    <?php require_once '../extensiones/scripts.php' ?>
    

</body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes-a/tabla-a.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#addarea').click(function(){  
                // alert('correcto');
                // are_nombre=$('#are_nombre').val();
                // pyt_id=$('#pyt_id_a').val();
                // addArea(are_nombre, pyt_id);
                addArea(); 
           });


           $('#editararea').click(function(e){
                // alert('click');   
            e.preventDefault();
            modificararea();
           });




           


        });
    </script>



