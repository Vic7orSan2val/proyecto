function addArea(){   
    var are_nombre = document.getElementById('are_nombre').value;
    var pyt_id = document.getElementById('pyt_id_a').value;
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var novale = /^\s/; //NO ESPACIO AL PRINCIPIO

    // var expresion = /\w+@\w+.\+[a-z]/; //expresion correo
    if(are_nombre ==="" && pyt_id==="0"){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false; 
    }
    else if(are_nombre ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar un area de trabajo'           
          })
          return false; 
    }
    else if(novale.test(are_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Area de trabajo no puede tener espacio en blanco al inicio'           
          })
          return false; 
    }    
    else if(!expresion.test(are_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Area de trabajo solo permite letras'           
          })
          return false; 
    }
    else if(pyt_id === "0" || pyt_id ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un proyecto por favor'           
          })
          return false; 
    }
    else{
        // alert('bien');
        cadena = "are_nombre=" + are_nombre + "&pyt_id=" + pyt_id ;
    
        $.ajax({
            type: "POST",
            url: "crud/agregar-area.php",
            data:  cadena,
            success: function(r){
                if(r==1){
                    $('#tabla').load('componentes-a/tabla-a.php');
                    //alert("Agregado con exito");
                    Swal.fire({
                        icon: 'success',
                        title: 'Registro exitoso',
                        text: "La marca se ha registrado con éxito",
    
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Continuar'
                    }).then((result) => {
                        if (result.value) {
                            location.href = "../area/lista-area.php";
                        }
                    })
                }
                else{
                   // alert("ERROR, NO AGREGADO");
                    Swal.fire({
                        icon: 'error',
                        title: 'Ha ocurrido un error',
                        text: 'Debe completar todos los campos'
                    })
                }
            }
        });
    }
    }

    // function datosform(datos){
       
    //     d = datos.split('||');
    //     $('#are_id').val(d[0]);
    //     $('#pyt_id_e').val(d[1]);
    //     $('#are_nombre_e').val(d[2]); 
   
    // }
    
    // function modificararea(){ 
    //     // are_id = $('#are_id').val(),
    //     // pyt_id=  $('#pyt_id_e').val(),
    //     // are_nombre = $('#are_nombre_e').val()
   
    //     var datos = $('#formulario_editar_area').serialize();
    
    //     // cadena =    "are_id="+are_id+
    //     //             "&pyt_id=" + pyt_id +                   
    //     //             "&are_nombre="+are_nombre;
                    
    //                 $.ajax({
    //                     type: "POST",
    //                     url: "crud/editar-a.php",
    //                     data:  datos,
    //                     success: function(r){
    //                         if(r==1){ 
    //                             $('#tabla').load('componentes-a/tabla-a.php');
    //                             //alert("Agregado con exito");
    //                             Swal.fire({
    //                                 icon: 'success',
    //                                 title: 'Registro exitoso',
    //                                 text: "La marca se ha registrado con éxito",
                
    //                                 showCancelButton: false,
    //                                 confirmButtonColor: '#3085d6',
    //                                 cancelButtonColor: '#d33',
    //                                 confirmButtonText: 'Continuar'
    //                             }).then((result) => {
    //                                 if (result.value) {
    //                                     location.href = "../area/lista-area.php";
    //                                 }
    //                             })
    //                         }
    //                         else{
    //                            // alert("ERROR, NO AGREGADO");
    //                             Swal.fire({
    //                                 icon: 'error',
    //                                 title: 'Ha ocurrido un error',
    //                                 text: 'Debe completar todos los campos'
    //                             })
    //                         }
    //                     }
    //                 });
    //                 return false;
                    
    //             }
                // ELIMINAR
function confirmarEliminar(are_id){
    
    // alertify.confirm('Eliminar Area de Trabajo','¿Seguro que desea eliminar?', 
    // function()
    // {   eliminarProyecto(are_id)},
    //     // alertify.success('Si') } ,
    // function(){
    //     alertify.message('Eliminación cancelada')});
    // // alert('hola')
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
        })
    
        swalWithBootstrapButtons.fire({
        title: '¿Está seguro que desea eliminar el area de trabajo?',
        text: "No podrá recuperar el registro guardado!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: '¡Sí, eliminar!',
        cancelButtonText: '¡No, cancelar!',
        reverseButtons: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        }).then((result) => {
        if (result.value) {
            eliminarProyecto(are_id)
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
            'Cancelado',
            'El area de trabajo no se eliminará',
            'error'
            )
        }
        })
}

function eliminarProyecto(are_id){
cadena="are_id=" + are_id;

$.ajax({
    type: "POST", 
    url: 'crud/eliminar-a.php' ,
    data: cadena ,
    success: function(r){
        if(r==1){
            $('#tabla').load('componentes-a/tabla-a.php');
            // alertify.success("Eliminado con exito");
            Swal.fire({
                icon: 'success',
                title: 'Eliminación exitosa',
                text: "Area de trabajo eliminada",

                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continuar'
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            })
            
        }else{
            // alertify.error("Error");
            Swal.fire({
                icon: 'error',
                title: 'No se ha podido eliminar',
                text: 'Ocurrió un error interno'
            })

        }
    }
});
}