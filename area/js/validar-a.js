function validarArea(){
    var are_nombre = document.getElementById('are_nombre').value;
    var pyt_id = document.getElementById('pyt_id').value;
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    var novale = /^\s/; //NO ESPACIO AL PRINCIPIO

    // var expresion = /\w+@\w+.\+[a-z]/; //expresion correo
    if(are_nombre ==="" || pyt_id===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false; 
    }
    else if(are_nombre ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Debe ingresar un area de trabajo'           
          })
          return false; 
    }
    else if(novale.test(are_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Area de trabajo no puede tener espacio en blanco al inicio'           
          })
          return false; 
    }    
    else if(!expresion.test(are_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Area de trabajo solo permite letras'           
          })
          return false; 
    }
    else if(pyt_id === "0" || pyt_id ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un proyecto por favor'           
          })
          return false; 
    }else{
        Swal.fire({
            icon: 'success',
            title: 'Modificacion exitosa...',
            text: 'La modificacion se ha realizado con exito'           
          })
    }
}