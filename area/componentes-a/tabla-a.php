<?php 
    include '../../config/conexion.php';
    $sql = "SELECT a.are_id, a.are_nombre, p.pyt_nombre 
            FROM are_area a JOIN pyt_proyecto p ON a.pyt_id = p.pyt_id";
    $resultado = conexionbd()->query($sql);

    // Creación del array con los datos 
    $resultado2 = mysqli_query($conexion,$sql) or die (mysql_error ());

    $areas = array();
    
    while( $rows = mysqli_fetch_assoc($resultado2) ) {
    
    $areas[] = $rows;
    
    }

    // Función que crea el excel

    if(isset($_POST["export_data"])) {

        if(empty($areas)) {?>
            <script>
            alert('NO HAY DATOS PARA EXPORTAR')
            window.location = "../lista-area.php"
            </script>
            
      <?php  }else{
            
            $filename = "Areas-trabajo.xls";
            
            header("Content-Type: application/vnd.ms-excel");
            
            header("Content-Disposition:attachment; filename=".$filename);
            
            $mostrar_columnas = false;
    
            foreach($areas as $area) {
    
                if(!$mostrar_columnas) {
    
                    echo implode("\t", array_keys($area)) . "\n";
                    $mostrar_columnas = true;
                }
            echo implode("\t", array_values($area)) . "\n";
            }
        }
    exit;
    }
    
?>

 <!-- Inicio tabla -->
<div class="row">
    <div class="table-responsive">
        <caption>
        <!-- <a href="#" class="btn btn-block btn-success">Agregar</a> -->
        <button class="col-sm-4 btn btn-block btn-success"data-toggle="modal" data-target="#m_add_area">Agregar </button>
        </caption><br>
        <table class="table table-bordered" id="datatable">
            <thead class="thead-dark">
                <tr>
                    <th>Area de trabajo</th>
                    <th>Proyecto</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($resultado->num_rows>0):
                    while($area = $resultado->fetch_assoc()):

                        // $datos = $area['are_id']."||".$area['pyt_nombre']."||".$area['are_nombre'];
                        // $datos = $area['are_id']."||".$area['pyt_nombre']."||".$area['are_nombre'];
                       
                ?>
                <tr>
                    <td><?php echo $area['are_nombre'] ?></td>
                    <td><?php echo $area['pyt_nombre']?></td>
                   
                    <td>
                        <div class="row">
                            <!-- <div class="col-md-6">                                
                                <a href="#" class="btn btn-block btn-info" data-toggle="modal"
                                 data-target="#m_editar_area" onclick="datosform('<?php // echo $datos['are_id']?>')">Modificar</a>
                            </div> -->
                            <div class="col-md-6">
                                <a href="form-editar-area.php?id_a=<?php echo $area['are_id'];?>" class="btn btn-block btn-info">Modificar</a>
                                
                            </div>

                            <div class="col-md-6">
                                <!-- <button type="submit" onclick="confirmar(<?php //echo $area['are_id'] ?>)" class="btn btn-block btn-danger">Eliminar</button> -->
                                <button type="submit" class="btn btn-block btn-danger" onclick="confirmarEliminar(<?php echo $area['are_id']?>)">Eliminar</button>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php 
                    endwhile;
                endif;
                ?>
            </tbody>
        </table>
    </div>
</div>



                <!-- Fin tabla -->

<div class="btn-group pull-right">
    <form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data'
            value="Export to excel" class="btn btn-info">Exportar a Excel</button>
    </form>
</div> 
                
  