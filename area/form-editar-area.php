<?php require_once '../sesiones/sesion.php';?>

<?php 
    require_once '../config/conexion.php';
    
    $modificar = $_GET['id_a'];
    $sql = "SELECT a.are_id, a.are_nombre, p.pyt_nombre ,p.pyt_id
    FROM are_area a JOIN pyt_proyecto p ON a.pyt_id = p.pyt_id
    WHERE are_id = '$modificar'";

    $resultado = conexionbd()->query($sql);

    $datos = $resultado->fetch_array();

?>

<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Areas de trabajo</title>
    <?php 
    require_once '../config/conexion.php';
    require_once '../extensiones/head.php' ;
    require_once '../extensiones/scripts.php' ;
    require_once '../extensiones/nav_jefecuadrilla.php' ;?>
    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funciones-a.js"></script>
    <script src="js/validar-a.js"></script>
</head>
<body> 
<div class="container mb-5 mt-5">
<h3 class="mb-4">Editar Area</h3>
<form action="crud/editar-area.php" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validarArea();">
                            
                            <input type="text" hidden="" name="id_area" id="id_area" value="<?php echo $modificar?>">

                            <div class="row">
                               <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="are_nombre" name="are_nombre" value="<?php echo $datos['are_nombre']?>"> 
                                    </div>
                               </div>        
                                                  
                               <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_tpy = "SELECT * FROM pyt_proyecto";
                                    $res = conexionbd()->query($sql_modificar_tpy);                                    
                                ?>
                                    <select class="form-control"name="pyt_id" id="pyt_id">
                                        <option value="" disabled="">Proyecto</option>                                                                       
                                        <?php 
                                            if($filas = $res->num_rows>0){
                                                while($pyt_id = $res->fetch_assoc()){ 
                                                    if($datos ['pyt_id'] == $pyt_id['pyt_id']){?>
                                                    <option selected="selected" value="<?php echo $pyt_id['pyt_id'];?>"><?php echo $pyt_id['pyt_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $pyt_id['pyt_id'];?>"> <?php echo $pyt_id['pyt_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>  
                           </div> 
                           <div class="row">
                                <a href="#" onclick="confirmarC()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>
                                <script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="lista-area.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>

                                <button id="editar_area" name="editar_area" style="margin: auto;" type="submit" class="btn btn-primary col-md-5" >Guardar Cambios</button>
                           </div>                         
                       </form>  
</div>  




</body>
</html>