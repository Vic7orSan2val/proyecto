<?php require_once '../sesiones/sesion.php';?>
<?php
    require_once '../config/conexion.php';     
    $modificar = $_GET['id'];
    $sql= "SELECT mtr.mtr_id, tmt.tmt_nombre, emt.emt_nombre, inv.inv_id, 
    mtr.mtr_nombre, mtr.mtr_detalle, mtr.mtr_cantidad, tmt.tmt_id, emt.emt_id,inv.inv_id
            FROM mtr_material mtr 
            LEFT JOIN tmt_tipo_material tmt ON mtr.tmt_id = tmt.tmt_id
            LEFT JOIN emt_estado_material emt ON mtr.emt_id = emt.emt_id
            LEFT JOIN inv_inventario inv ON mtr.inv_id = inv.inv_id
            WHERE mtr.mtr_id = '$modificar'";
    $resultado = conexionbd()->query($sql);
    $datos = $resultado->fetch_array();
?>
<!DOCTYPE html>
<html lang="en">
<head>    
    <title>Editar Material</title>
    <?php   require_once '../extensiones/head.php';            
            require_once '../extensiones/nav_jefeinventario.php' ;
            require_once '../extensiones/scripts.php' ;
    ?>    
    <link rel="stylesheet" href="../css/estilos.css">   
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <!-- <script src="js/funciones-proyecto.js"></script> --> 
    <!-- <script src="js/validar-proyecto.js"></script> -->
    <script src="js/validar-m.js"></script>
</head>
<body>
<div class="container mt-5 mb-5">                   
<h3 class="mb-4">Editar Material</h3>
<form action="crud/editar-materiales.php" method="POST" id="formulario_agregar" name="formulario_agregar" onsubmit="return validarM()">
                            <div class="col-sm-12">
                               <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_id" name="mtr_id" value="<?php echo $datos['mtr_id']?>">
                               </div>
                            </div> 
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_nombre" name="mtr_nombre" value="<?php echo $datos['mtr_nombre']?>">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_detalle" name="mtr_detalle" value="<?php echo $datos['mtr_detalle']?>">
                                </div>
                            </div> 
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_cantidad" name="mtr_cantidad" value="<?php echo $datos['mtr_cantidad']?>">
                                </div>
                            </div>                             
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_tmt = "SELECT * FROM tmt_tipo_material";
                                    $res_tmt = conexionbd()->query($sql_modificar_tmt);                                    
                                ?>
                                    <select class="form-control"name="tmt_id" id="tmt_id">
                                        <option value="" disabled="">Seleccione Tipo de Material</option>                                                                       
                                        <?php 
                                            if($filas = $res_tmt->num_rows>0){
                                                while($tmt_id = $res_tmt->fetch_assoc()){ 
                                                    if($datos['tmt_id'] == $tmt_id['tmt_id']){?>
                                                    <option selected="selected" value="<?php echo $tmt_id['tmt_id'];?>"><?php echo $tmt_id['tmt_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $tmt_id['tmt_id'];?>"> <?php echo $tmt_id['tmt_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>                            
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_emt = "SELECT * FROM emt_estado_material";
                                    $res_emt = conexionbd()->query($sql_modificar_emt);                                    
                                ?>
                                    <select class="form-control"name="emt_id" id="emt_id">
                                        <option value="" disabled="">Estado de Material</option>                                                                       
                                        <?php 
                                            if($filas = $res_emt->num_rows>0){
                                                while($emt_id = $res_emt->fetch_assoc()){ 
                                                    if($datos['emt_id'] == $emt_id['emt_id']){?>
                                                    <option selected="selected" value="<?php echo $emt_id['emt_id'];?>"><?php echo $emt_id['emt_nombre'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $emt_id['emt_id'];?>"> <?php echo $emt_id['emt_nombre'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                <?php                                 
                                    $sql_modificar_emt = "SELECT * FROM inv_inventario";
                                    $res_inv = conexionbd()->query($sql_modificar_emt);                                    
                                ?>
                                    <select class="form-control"name="inv_id" id="inv_id">
                                        <option value="" disabled="">Seleccione Inventario de Proyecto</option>                                                                       
                                        <?php 
                                            if($filas = $res_inv->num_rows>0){
                                                while($inv_id = $res_inv->fetch_assoc()){ 
                                                    if($datos['inv_id'] == $inv_id['inv_id']){?>
                                                    <option selected="selected" value="<?php echo $inv_id['inv_id'];?>"><?php echo $inv_id['inv_id'];?></option>
                                                <?php }else{ ?>
                                                    <option value="<?php echo $inv_id['inv_id'];?>"> <?php echo $inv_id['inv_id'];?></option>
                                                <?php }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>                                                                         
                           <div class="row">
                                <a href="#" onclick="confirmarC()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>

                                <button id="editar" name="editar" style="margin: auto;" type="submit" class="btn btn-primary col-md-5" >Guardar cambios</button>
                           </div>                          
                       </form>                
</div>
</body>
</html>
<script>
                    function confirmarC()
                    {                        
                        alertify.confirm('Cancelar modificacion',"¿Desea cancelar la modificacion?",
                        function(e){
                          if(e){
                            alert('Modificacion Cancelada')
                            window.location="lista-materiales.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Siga editando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>