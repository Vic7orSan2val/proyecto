function validarM(){
    var mtr_id = document.getElementById('mtr_id').value;
    var mtr_nombre = document.getElementById('mtr_nombre').value;
    var mtr_detalle= document.getElementById('mtr_detalle').value;
    var mtr_cantidad= document.getElementById('mtr_cantidad').value;
    var tmt_id= document.getElementById('tmt_id').value;
    var emt_id= document.getElementById('emt_id').value;
    var inv_id= document.getElementById('inv_id').value;
    var novale = /^\s/; //NEGACION DEL ESPACIO AL PRINCIPIO
    var expresioncn = /[a-zA-ZÀ-ÿ]/;
    var expresion = /[a-zA-ZÀ-ÿ]$/;
    if(mtr_id === "" && mtr_nombre === ""&& mtr_detalle=== "" && mtr_cantidad=== "" &&tmt_id=== "" &&
    emt_id=== "" && inv_id=== ""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete todos los campos por favor'           
          })
          return false;
    }else if(mtr_id ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese un codigo de material por favor'           
          })
          return false;        
    }else if(novale.test(mtr_id)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El codigo no puede comenzar con un espacio en blanco'           
          })
          return false;
    }else if(isNaN(mtr_id)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El codigo solo permite numeros'           
          })
          return false;
    }else if(mtr_nombre ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Ingrese un nombre de material por favor'           
          })
          return false;
    }else if(novale.test(mtr_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Nombre no puede comenzar con un espacio en blanco'           
          })
          return false;
    }else if(!expresion.test(mtr_nombre)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'El nombre solo puede contener letras'           
          })
          return false;
    }else if(mtr_detalle ===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete el campo de detalles por favor'           
          })
          return false;
    }else if(novale.test(mtr_detalle)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Campo detalles no puede comenzar con un espacio en blanco'           
          })
          return false;
    }else if(!expresioncn.test(mtr_detalle)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Campo detalles solo permite letras y numeros'           
          })
          return false;
    }else if(mtr_cantidad===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Complete el campo cantidad'           
          })
          return false;
    }else if(isNaN(mtr_cantidad)){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Cantidad solo permite numeros'           
          })
          return false;
    }else if(tmt_id==="0" || tmt_id===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un tipo de material'           
          })
          return false;
    }else if(emt_id==="0" || emt_id===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione estado de material'           
          })
          return false;
    }else if(inv_id==="0" || inv_id===""){
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'Seleccione un inventario'           
          })
          return false;
    }else{
        Swal.fire({
            icon: 'success',
            title: 'Modificacion Exitosa',
            text: 'Los registros se guardaron correctamente'           
          })          
    }
}