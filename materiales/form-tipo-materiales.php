<?php require_once '../sesiones/sesion.php';?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php require '../extensiones/head.php' ?>
    <title>Registrar materiales</title>
    <link rel="shortcut icon" type="image/x-icon" href="../img/materiales.png">
</head>

<body>
    <?php require '../extensiones/nav-trabajadores.php' ?>

    <div class="contenedor">

        <h2 class="form_titulo">Formulario <span> Registro de materiales</span></h2>

        <div class="form">
            <form action="agregar-materiales.php" method="POST">
                <p>
                    <label for="id_material">Codigo Producto</label>
                    <input type="tel" placeholder="Ingrese el codigo del producto" id="id_material" name="id_material" max="12" maxlength="12" required>

                </p>
                <p>
                    <label for="material">Material</label>
                    <input type="text" placeholder="Ingrese el nombre del material" id="material" name="material" maxlength="12" required>
                </p>
                <p>
                    <label for="cantidad_material">Cantidad</label>
                    <input type="tel" placeholder="Ingrese la cantidad de materiales" id="cantidad_material" name="cantidad_material" max="12" maxlength="12" required>
                </p>


                <button type="submit" class="button2">Registrar Material</button>

            </form>



        </div>
    </div>

    <?php require '../extensiones/scripts.php' ?>
</body>

</html>