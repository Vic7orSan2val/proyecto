<?php require_once '../sesiones/sesion.php';?>

<?php 
    require_once '../config/conexion.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>   
    <title>Administracion de Materiales</title>
    <?php require_once '../extensiones/head.php' ?>
    <?php require_once '../extensiones/nav_jefeinventario.php' ?>
    <link rel="stylesheet" href="../css/estilos.css">  
    <!-- <script src="../librerias/jquery-3.5.1.min.js"></script> -->
    <script src="js/funciones-m.js"></script>
    <!-- <script src="js/validar-proyecto.js"></script> -->
</head>
<body>
    <div class="ml-5 mr-5">
        <div class="menu">
            <div class="row"> 
                <!-- Inicio titulo -->
                <div class="col-md-12">    
                    <div class="titulo">                                                
                        <h4>Administracion de Materiales</h4><br>                        
                    </div>
                </div>
                <!-- Fin titulo -->

                <!-- BUSCADOR -->
                
                <!-- <div id="buscador" class="col-md-12"></div><br>
                <br> -->
                <br>
                <!--LLAMADA TABLA -->
                    <div class="col-md-12" id="tabla"></div>
                <!-- FIN LLAMADA TABLA --> 

 

                                    <!--MODAL AGREGAR -->
<div class="modal fade" id="m_agregar_material" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Agregar Nuevo Material</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class="container ">                   
                       <form action="" method="POST" id="formulario_agregar" name="formulario_agregar">
                            <div class="col-sm-12">
                               <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_id" name="mtr_id" placeholder="Ingrese el codigo del material">
                               </div>
                            </div> 

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_nombre" name="mtr_nombre" placeholder="Ingrese nombre del material">
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_detalle" name="mtr_detalle" placeholder="Ingrese detalles del material">
                                </div>
                            </div>
 
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input class="form-control" type="text" id="mtr_cantidad" name="mtr_cantidad" placeholder="Ingrese cantidad">
                                </div>
                            </div>
                             
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php 
                                        $tipo_m = "SELECT * FROM tmt_tipo_material";
                                        $result_m = conexionbd()->query($tipo_m);
                                    ?>
                                    <select class="form-control"name="tmt_id" id="tmt_id">
                                        <option value="0" disabled selected>Seleccione Tipo Material</option>
                                        <?php 
                                            while($rowtm = $result_m->fetch_assoc()){?>
                                                <option value="<?php echo $rowtm['tmt_id']?>"> <?php echo $rowtm['tmt_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php 
                                        $estado_m = "SELECT * FROM emt_estado_material";
                                        $result_estado = conexionbd()->query($estado_m);
                                    ?>
                                    <select class="form-control"name="emt_id" id="emt_id">
                                        <option value="0" disabled selected>Seleccione Estado Material</option>
                                        <?php 
                                            while($rowestado = $result_estado->fetch_assoc()){?>
                                                <option value="<?php echo $rowestado['emt_id']?>"> <?php echo $rowestado['emt_nombre']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?php 
                                        $inventario = "SELECT * FROM inv_inventario";
                                        $result_inv = conexionbd()->query($inventario);
                                    ?>
                                    <select class="form-control"name="inv_id" id="inv_id">
                                        <option value="0" disabled selected>Seleccione Id Inventario</option>
                                        <?php 
                                            while($rowinv = $result_inv->fetch_assoc()){?>
                                                <option value="<?php echo $rowinv['inv_id']?>"> <?php echo $rowinv['inv_id']?></option>                                                
                                        <?php } ?>                                                                        
                                    </select>
                                </div>
                            </div>
                                                                         

                           <div class="row">
                                <a href="#" onclick="confirmarR()" id="cancelarproyecto" name="cancelarproyecto" style="margin: auto;" type="button" class="btn btn-danger col-md-5">Cancelar</a>

                                <button id="addMat" name="addMat" style="margin: auto;" type="button" class="btn btn-primary col-md-5" >Registrar</button>
                           </div>
                          
                       </form>
                   
        </div>
      </div>
                <div class="modal-footer" >
      
                </div>
    </div>
  </div>
</div>




           
            </div>
        </div>
    </div>

    
    <?php require_once '../extensiones/scripts.php' ?>
    

</body>
</html>

<script type="text/javascript">
    $(document).ready(function(){
        $('#tabla').load('componentes-m/tabla-m.php');
       // $('#buscador').load('componentes/buscador.php');
    });
</script>

<script type="text/javascript">
        $(document).ready(function () {  
           $('#addMat').click(function(){  
                // alert('correcto');
                // mtr_id=$('#mtr_id').val();
                // mtr_nombre=$('#mtr_nombre').val();
                // mtr_detalle=$('#mtr_detalle').val();                
                // mtr_cantidad=$('#mtr_cantidad').val();
                // tmt_id=$('#tmt_id').val();
                // emt_id=$('#emt_id').val();
                // inv_id=$('#inv_id').val();                
                agregardatos();
           });          

        });
    </script>



<script>
                    function confirmarR()
                    {                        
                        alertify.confirm('Cancelar Registro',"¿Desea cancelar el registro?",
                        function(e){
                          if(e){
                            alert('Registro Cancelado')
                            window.location="lista-materiales.php"
                            
                          }                                                 
                        },
                        function(){
                          alertify.message('Puede seguir agregando');
                        }).set('labels', {ok:'Aceptar', cancel:'Cancelar'});
                        
                    }
                </script>