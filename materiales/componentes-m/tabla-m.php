<?php 
    include '../../config/conexion.php';
    require_once '../../extensiones/head.php';
    require_once '../../extensiones/scripts.php';
         
        $sql= "SELECT mtr.mtr_id, tmt.tmt_nombre, emt.emt_nombre, inv.inv_id, 
        mtr.mtr_nombre, mtr.mtr_detalle, mtr.mtr_cantidad
                FROM mtr_material mtr 
                LEFT JOIN tmt_tipo_material tmt ON mtr.tmt_id = tmt.tmt_id
                LEFT JOIN emt_estado_material emt ON mtr.emt_id = emt.emt_id
                LEFT JOIN inv_inventario inv ON mtr.inv_id = inv.inv_id";

        $resultado = conexionbd()->query($sql);

        
        // Creación del array con los datos 
        $resultado2 = mysqli_query($conexion,$sql) or die (mysql_error ());

        $materiales = array();
        
        while( $rows = mysqli_fetch_assoc($resultado2) ) {
        
        $materiales[] = $rows;
        
        }

        // Función que crea el excel
        if(isset($_POST["export_data"])) {

            if(empty($materiales)) { ?>
                <!-- echo "No hay datos a exportar"; -->
                <script>
                    alert('NO EXISTEN DATOS PARA EXPORTAR')
                    window.location = "../lista-materiales.php"
                </script>
              <?php 
            }else{
                
                $filename = "Materiales.xls";
                
                header("Content-Type: application/vnd.ms-excel");
                
                header("Content-Disposition:attachment; filename=".$filename);
                
                $mostrar_columnas = false;
        
                foreach($materiales as $material) {
        
                    if(!$mostrar_columnas) {
        
                        echo implode("\t", array_keys($material)) . "\n";
                        $mostrar_columnas = true;
                    }
                echo implode("\t", array_values($material)) . "\n";
                }
            }
        exit;
        }
    
?>


 <!-- Inicio tabla -->
<div class="row">
    <div class="table-responsive">
        <caption>
        <!-- <a href="#" class="btn btn-block btn-success">Agregar</a> -->
        <button class="col-sm-4 btn btn-block btn-success"data-toggle="modal" data-target="#m_agregar_material">Agregar Material</button>
        </caption><br>
        <table class="table table-bordered" id="datatable">
            <thead class="thead-dark">
                <tr>
                    <th>Codigo</th>
                    <th>Tipo Material</th>
                    <th>Estado Material</th>                
                    <th>Inventario ID</th>
                    <th>Nombre Material</th>
                    <th>Detalle Material</th>
                    <th>Cantidad</th>                
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if($resultado->num_rows>0):
                    while($material = $resultado->fetch_assoc()):

                        //$datos = $material['tpy_id']."||".$material['tpy_nombre']."||".$material['tpy_descripcion'];
                       $datos =     $material['mtr_id']."||".
                                    $material['tmt_nombre']."||".
                                    $material['emt_nombre']."||".
                                    $material['inv_id']."||".                                    
                                    $material['mtr_nombre']."||".
                                    $material['mtr_detalle']."||".
                                    $material['mtr_cantidad'];
                ?>
                <tr>
                    
                    <td><?php echo $material['mtr_id'] ?></td>
                    <td><?php echo $material['tmt_nombre'] ?></td>
                    <td><?php echo $material['emt_nombre']?></td>
                    <td><?php echo $material['inv_id']?></td>                    
                    <td><?php echo $material['mtr_nombre']?></td>
                    <td><?php echo $material['mtr_detalle']?></td>
                    <td><?php echo $material['mtr_cantidad']?></td>                                
                                    
                    <td>
                        <div class="row">
                            <div class="col-sm-6">                                
                                <a class="btn btn-block btn-info" href="form-editar-material.php?id=<?php echo $material['mtr_id']?>">Modificar</a>
                            </div>

                            <div class="col-sm-6">
                                <!-- <button type="submit" onclick="confirmar(<?php //echo $material['tpy_id'] ?>)" class="btn btn-block btn-danger">Eliminar</button> -->
                                <button type="submit" class="btn btn-block btn-danger" onclick="confirmarEliminar(<?php echo $material['mtr_id']?>)">Eliminar</button>
                        </div>
                    </td>
                </tr>
                <?php 
                    endwhile;
                endif;                
                ?>
            </tbody>
        </table>
    </div>
</div>



                <!-- Fin tabla -->

<div class="btn-group pull-right">
    <form action=" <?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
        <button type="submit" id="export_data" name='export_data'
            value="Export to excel" class="btn btn-info">Exportar a Excel</button>
    </form>
</div>         
                <!-- Botón que genera el excel -->
                
  