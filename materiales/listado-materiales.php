<?php
require_once("../config/db.php"); //Contiene las variables de configuracion para conectar a la base de datos
require_once("../config/conexion.php"); //Contiene funcion que conecta a la base de datos
$sql_materiales = "SELECT * FROM materiales";
$consulta_materiales = $con->query($sql_materiales);
?>

<!DOCTYPE html>
<html lang="en">

<head> 
    <?php require '../extensiones/head.php' ?>
    <title>Listado de materiales</title>
    <link rel="shortcut icon" type="image/x-icon" href="../img/materiales.png">
</head>

<body style="background:linear-gradient(20deg, LightGray, Gainsboro);";>
    <?php require '../extensiones/nav-trabajadores.php' ?>

    <div class="panel-tablas">
    <div class="table-responsive">
        <h2 class="form_titulo">Listado materiales</h2>
        <table id="datatable" class="table table-bordered ">

            <thead class="thead-dark">
                <tr class="danger">
                    <th scope="col">Codigo Material</th>
                    <th scope="col">Nombre material</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($consulta_materiales->num_rows > 0) {
                    while ($materiales = $consulta_materiales->fetch_assoc()) {
                ?>

                        <tr>

                            <td><?php echo $materiales['id_material'] ?></td>
                            <td><?php echo $materiales['material'] ?></td>
                            <td><?php echo $materiales['cantidad_material'] ?></td>
                            <td>
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="modificar-material.php?codmat=<?php echo $materiales['id_material'] ?>" class="btn btn-block" style="background: deepskyblue; color: white">Editar</a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="eliminar-material.php?codmat=<?php echo $materiales['id_material'] ?>" class="btn btn-block" style="background: red; color: white">Eliminar</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                <?php }
                } ?>
            </tbody>
        </table>
        <form action="form-tipo-materiales.php">
            <div>
                <button type="submit" class="button">Nuevo Registro</button>
            </div>

        </form>
    </div>
    </div>
    

    <div class="row">
        <div class="col-md-12">
            <?php
            if (!empty($_GET['error'])) {
                $respuesta = $_GET['error'];
                $contenido = $_GET['contenido']; ?>
                <?php if ($respuesta == 'vacio') { ?>
                    <div class="col-md-12">
                        <div class="alert alert-success" role="alert">
                            <?php echo $contenido ?>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <?php
            if (!empty($_GET['error'])) {
                $respuesta = $_GET['error'];
                $contenido = $_GET['contenido'];
            ?>
                <?php
                if ($respuesta == 'modificado') { ?>
                    <div class="alert alert-primary" role="alert">
                        <?php echo $contenido ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <?php
            if (!empty($_GET['error'])) {
                $respuesta = $_GET['error'];
                $contenido = $_GET['contenido'];
            ?>

                <?php
                if ($respuesta == 'eliminado') { ?>
                    <div class="col-md-12">
                        <div class="alert alert-danger" role="alert">
                            Material Eliminado
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>


    </div>

    <?php require '../extensiones/scripts.php' ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable({
                language: {
                    search: "Buscar:",
                    paginate: {
                        first: "Primer",
                        previous: "Anterior",
                        next: "Siguiente",
                        last: "Último"
                    },
                    info: "Mostrando del _START_ al _END_ de _TOTAL_ resultados disponibles",
                    emptyTable: "No existen elementos para mostrar en la tabla",
                    infoEmpty: "Mostrando del 0 al 0 de 0 resultados",
                    infoFiltered: "(Filtrado de _MAX_ resultados)",
                    lengthMenu: "Mostrando _MENU_ resultados",
                    loadingRecords: "Cargando...",
                    processing: "Procesando...",
                    zeroRecords: "No se encontraron resultados",
                    aria: {
                        sortAscending: ": Ordenado de forma ascendente",
                        sortDescending: ": Ordenado de forma descendente"
                    }

                }
            });
        });
    </script>


</body>

</html>